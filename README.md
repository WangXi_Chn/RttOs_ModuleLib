# RttOs_ModuleLib

## 介绍

Module library based on RT-Thread operate system

## 软件架构

基于RTT操作系统的模块库
可作为辅助工具使用提高代码利用率

## 使用说明

1. 添加文件目录的路径至MDK中
2. 添加需要的模块文件在工程树下
3. 按照头文件中的函数功能使用

## 代码特性

1. 面向对象编程，除信号量必须要求外，无全局变量
2. 使用简单
3. 可扩展性强
4. 各个模块之间的耦合程度极低

## 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
