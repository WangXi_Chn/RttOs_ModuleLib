# RttOs_ModuleLib

## introduce

Module library based on RT-Thread operate system

## Software architecture

Module library based on RTT operating system
Can be used as an auxiliary tool to improve code utilization

## Instructions for use

1. Add the path of file directory to MDK
2. Add the required module files under the project tree
3. Follow the functions in the header file

## Code features

1. Object-oriented programming, no global variables except semaphore requirements
2. Easy to use
3. Strong scalability
4. The degree of coupling between each module is extremely low

## Code cloud special effects

1. Use Readme\ _xxx.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2. Official Code Cloud blog [blog.gitee.com]
3. You can [https://gitee.com/explore]this address to decode the cloud good open source project
4. [GVP](https://gitee.com/gVP) is the most valuable open source project of code cloud and an excellent open source project comprehensively evaluated by code cloud
5. User Manual provided by code Cloud official [https://gitee.com/help](https://gitee.com/help)
6. The cover character of code cloud is a column used to show the charm of code cloud members.
