/*
 * Copyright (c) 2020 - ~, HIT_HERO Team
 *
 * KALMAN FILTER MATH SOURCE FILE
 * Used in RT-Thread Operate System
 *
 * Change Logs:
 * Date           Author       Notes			Mail
 * 2020-09-04     WangXi   	   first version	WangXi_Chn@foxmail.com
 */
 
#include "Math_Kalman.h"

static void Module_KalmanInit(MATH_KALMANFILTER* module);
static void Module_KalmanUpdate(MATH_KALMANFILTER* module,float Input);

void Module_KalmanFilter_Config(MATH_KALMANFILTER *module)
{
	
	if(	module->Method_Init 	== NULL &&
		module->Method_Update	== NULL
		
	){	
		/* Link the Method */
		module->Method_Init		= Module_KalmanInit;
		module->Method_Update	= Module_KalmanUpdate;
		
	}
	else{
		rt_kprintf("Warning: Module MPU9250 is Configed twice\r\n");
		return;
	}

	/* Device Init */
	module->Method_Init(module);
	
	return;
}

void Module_KalmanInit(MATH_KALMANFILTER* module)
{
	module->Value_Now_P 	= 0;
	module->Value_Kg 		= 0;
	module->Value_out 		= 0;

}

void Module_KalmanUpdate(MATH_KALMANFILTER* module, float Input)
{
	//预测状态方程
	module->Value_out 		= module->Value_out;

	//预测协方差方程
	module->Value_Now_P 	= module->Property_Last_P + module->Property_Q;

	//卡尔曼增益方程
	module->Value_Kg 		= module->Value_Now_P / (module->Value_Now_P + module->Property_R);

	//更新最优值方程
	module->Value_out 		= module->Value_out + module->Value_Kg * (Input-module->Value_out);

	//更新协方差方程
	module->Property_Last_P = (1-module->Value_Kg) * module->Value_Now_P;

}

/************************ (C) COPYRIGHT 2020 WANGXI **************END OF FILE****/
