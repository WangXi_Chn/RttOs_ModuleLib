/*
 * Copyright (c) 2020 - ~, HIT_HERO Team
 *
 * KALMAN FILTER MATH SOURCE FILE
 * Used in RT-Thread Operate System
 *
 * Change Logs:
 * Date           Author       Notes			Mail
 * 2020-09-04     WangXi   	   second version	WangXi_Chn@foxmail.com
 */
 
#ifndef __KALMAN_MATH_H__
#define __KALMAN_MATH_H__

#include <rtthread.h>
#include <rtdevice.h>
#include <board.h>

struct _MATH_KALMANFILTER
{
	/* Property */
	float Property_Last_P;	//�ϴι���Э����
	float Property_Q;		//��������Э����
	float Property_R;		//�۲�����Э����
	
	/* Value */
	float Value_Now_P;		//��ǰ����Э����
	float Value_Kg;			//����������
	float Value_out;		//�������˲������
	
	/* Method */
	void (*Method_Init)(struct _MATH_KALMANFILTER *module);
	void (*Method_Update)(struct _MATH_KALMANFILTER *module,float Input);
};
typedef struct _MATH_KALMANFILTER MATH_KALMANFILTER;

void Module_KalmanFilter_Config(MATH_KALMANFILTER *module);


#endif
/************************ (C) COPYRIGHT 2020 WANGXI **************END OF FILE****/

