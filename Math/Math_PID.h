/*
 * Copyright (c) 2020 - ~, HIT_HERO Team
 *
 * PID MATH MODULE HEAD FILE
 * Used in RT-Thread Operate System
 *
 * Change Logs:
 * Date           Author       Notes            	Mail
 * 2020-08-05     WangXi       first version    	WangXi_chn@foxmail.com
 */
#include <rtthread.h>
#include <rtdevice.h>
#include <board.h>

#ifndef _MATH_PID_H_
#define _MATH_PID_H_

struct _MATH_PID
{
    /* Property */
	float Property_Kp;
	float Property_Ki;
	float Property_Kd;  

	float Property_dt;

	float Property_integralMax;
	float Property_integralErrMax;
	float Property_AimMax;
	float Property_OutputMax;

	
	/* Value */
	float Value_Aim;            
    float Value_Actual;         
    float Value_err;                 
    float Value_err_last;                            
    float Value_integral;
	
	float Value_output;
	
	/* Method */
	void (*Method_Init)(struct _MATH_PID *module);
	void (*Method_Update)(struct _MATH_PID *module,float Aim, float Feedback);
    
};
typedef struct _MATH_PID MATH_PID;


rt_err_t Module_PID_Config(MATH_PID *module);
















#endif

/************************ (C) COPYRIGHT 2020 WANGXI **************END OF FILE****/

