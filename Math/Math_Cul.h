/*
 * Copyright (c) 2020 - ~, HIT_HERO Team
 *
 * MATH CUL HEAD FILE
 * Used in RT-Thread Operate System
 *
 * Change Logs:
 * Date           Author       Notes			Mail
 * 2020-09-12     WangXi   	   first version	WangXi_Chn@foxmail.com
 */
 
#ifndef __MATHCUL_H__
#define __MATHCUL_H__

#include <math.h>

#define	MATH_MAX(x,y) (((x) > (y)) ? (x) : (y))
#define	MATH_MIN(x,y) (((x) < (y)) ? (x) : (y))

#define MATH_PI	3.1415926
#define MATH_RADToANG(Rad) Rad/MATH_PI*180.0
#define MATH_ANGToRAD(Ang) Ang/180.0*MATH_PI
#define MATH_COS_RAD(Rad) cos(Rad)
#define MATH_SIN_RAD(Rad) sin(Rad)
#define MATH_TAN_RAD(Rad) tan(Rad)
#define MATH_COS_ANG(Ang) cos(MATH_ANGToRAD(Ang))
#define MATH_SIN_ANG(Ang) sin(MATH_ANGToRAD(Ang))
#define MATH_TAN_ANG(Ang) tan(MATH_ANGToRAD(Ang))

#define MATH_ACCOS_RAD(Rate) acos(Rate)
#define MATH_ACSIN_RAD(Rate) asin(Rate)
#define MATH_ACTAN_RAD(Rate) atan(Rate)
#define MATH_ACCOS_ANG(Rate) MATH_RADToANG(acos(Rate))
#define MATH_ACSIN_ANG(Rate) MATH_RADToANG(asin(Rate))
#define MATH_ACTAN_ANG(Rate) MATH_RADToANG(atan(Rate))

#define MATH_ARRSIZE(a) (sizeof((a))/sizeof((a[0])))



#endif
/************************ (C) COPYRIGHT 2020 WANGXI **************END OF FILE****/

