/*
 * Copyright (c) 2020 - ~, HIT_HERO Team
 *
 * UARTMAVLINK MODULE HEAD FILE
 * Used in RT-Thread Operate System
 * Used in MAVLink Protocol
 *
 * Change Logs:
 * Date           Author       Notes                Mail
 * 2020-09-26     WangXi       first version        WangXi_chn@foxmail.com
 */
#ifndef _MODULE_UARTMAVLINK_H_
#define _MODULE_UARTMAVLINK_H_

#include <rtthread.h>
#include <rtdevice.h>
#include <board.h>

#include "mavlink.h"

typedef rt_uint8_t UARTMAVLINK_MYID;
typedef rt_uint8_t UARTMAVLINK_NETID;
typedef rt_uint8_t UARTMAVLINK_AIMID;
typedef rt_uint8_t UARTMAVLINK_GETID;

typedef enum{
	SIMPLEUART = 0,
	JC24B,
	
}UARTMAVLINK_DEVICE;

struct _MODULE_UARTMAVLINK
{
    /* Property */
    char *             		Property_UartDevName; 
    rt_uint32_t				Property_BaudRate;
	UARTMAVLINK_MYID 		Property_MyID;
	UARTMAVLINK_NETID		Property_NetID;
	UARTMAVLINK_DEVICE		Property_Device;
	
	rt_base_t 				Property_JC24BsetPin;          /*!< Only used when Property_Device is JC24B as set pin. >
										This parameter is defined by function @ref GET_PIN(GPIOPORT, GPIO_PIN_NUM) */
	
    /* Value */
    rt_device_t         	Value_uart_dev;

	UARTMAVLINK_AIMID 		Value_AimID;
	UARTMAVLINK_GETID 		Value_GetID;
	
	mavlink_message_t 		Value_mavlinksendmsg;
	mavlink_message_t 		Value_mavlinkgetmsg;
	mavlink_status_t  		Value_mavlinkstatus;
	
	struct rt_messagequeue  Value_mavlinkmsg_queue;
	mavlink_message_t		Value_mavlinkmsg_pool[8];
    
    /* Method */
    void (*Method_Init)(struct _MODULE_UARTMAVLINK *module);
    void (*Method_Send)(struct _MODULE_UARTMAVLINK *module);
    void (*Method_Feed)(struct _MODULE_UARTMAVLINK *module);
    rt_err_t (*Method_Handle)(rt_device_t dev, rt_size_t size);
    
};
typedef struct _MODULE_UARTMAVLINK MODULE_UARTMAVLINK;

rt_err_t Module_UartMavlink_Config(MODULE_UARTMAVLINK *module);


#endif
/************************ (C) COPYRIGHT 2020 WANGXI **************END OF FILE****/

