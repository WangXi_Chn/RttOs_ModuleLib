/*
 * Copyright (c) 2020 - ~, HIT_HERO Team
 *
 * KEY MODULE SOUCE FILE
 * Used in RT-Thread Operate System
 *
 * Change Logs:
 * Date           Author       Notes            	Mail
 * 2020-08-07     WangXi       first version    	WangXi_chn@foxmail.com
 */
 
#include "Module_Key.h"
/* User Code Begin*/

/* User Code End */

/* Static Method */
static void Module_KeyInit(MODULE_KEY *module);
static void Module_KeyUpdate(MODULE_KEY *module);
static void Module_KeyHandle(void *args);

static struct rt_semaphore pinKey_sem;

/* Global Method */
rt_err_t Module_Key_Config(MODULE_KEY *Dev_Key){
    if(	Dev_Key->Init==NULL &&
		Dev_Key->Handle==NULL &&
		Dev_Key->Update==NULL

    ){  
        /* Link the Method */
        Dev_Key->Init 	= Module_KeyInit;
		Dev_Key->Handle = Module_KeyHandle;
		Dev_Key->Update = Module_KeyUpdate;
    }
    else{
        rt_kprintf("Warning: Module Led is Configed twice\n");
        return RT_ERROR;
    }

    /* Device Init */
    Dev_Key->Init(Dev_Key);
    return RT_EOK;
}


/* Static Method */
static void Module_KeyHandle(void *args){
	rt_sem_release(&pinKey_sem);
}

static void Module_KeyUpdate(MODULE_KEY *module)
{
	rt_sem_take(&pinKey_sem, RT_WAITING_FOREVER);
	module->status ++;
}

static void Module_KeyInit(MODULE_KEY *module){
    rt_pin_mode(module->pin, PIN_MODE_INPUT_PULLUP);
	rt_pin_attach_irq(module->pin, PIN_IRQ_MODE_FALLING, Module_KeyHandle, RT_NULL);
	rt_pin_irq_enable(module->pin, PIN_IRQ_ENABLE);
	
	/* init semaphone */
	rt_sem_init(&pinKey_sem, "pinKey_sem", 0, RT_IPC_FLAG_FIFO);
}




/************************ (C) COPYRIGHT 2020 WANGXI **************END OF FILE****/

