/*
 * Copyright (c) 2020 - ~, HIT_HERO Team
 *
 * BLUETOOTH HC06 MODULE HEAD FILE
 * Used in RT-Thread Operate System
 *
 * Change Logs:
 * Date           Author       Notes                Mail
 * 2020-08-09     WangXi       first version        WangXi_chn@foxmail.com
 */
#include <rtthread.h>
#include <rtdevice.h>
#include <board.h>

#ifndef _MODULE_BLUETOOTHHC06_H_
#define _MODULE_BLUETOOTHHC06_H_

typedef enum funcFlag
{
    LED_BlUE_1  = 0xE1,
    LED_GREEN_1,
    LED_RED_1,
    LED_YELLOW_1,
    LED_BlUE_2,
    LED_GREEN_2,
    LED_RED_2,
    LED_YELLOW_2,
    
    PARAM_SHOW_1 = 0xC1,
    PARAM_SHOW_2,
    PARAM_SHOW_3,
    PARAM_SHOW_4,
    PARAM_SHOW_5,
    PARAM_SHOW_6,
    
    WAVE_SHOW = 0x01,
    
}FuncFlag;

enum ConnectStatusFlag
{
    DISCONNECTED = PIN_LOW,
    CONNECTED = PIN_HIGH,
    
};

struct _MODULE_BLUETOOTHHC06
{
    /* Property */
    char *              Property_UartDevName; 
    rt_base_t           ConnectStatusPin;
    rt_uint8_t          ConnectCheckSwitch;
    
    /* Value */
    rt_device_t         Value_uart_dev;
    rt_uint8_t          Value_sendData[8];
    rt_uint8_t          Value_feedData[8];
    
    rt_uint32_t         ConnectStatus;
    rt_uint16_t         Value_keyMask;
    
    /* Method */
    void (*Method_Init)(struct _MODULE_BLUETOOTHHC06 *module);
    void (*Method_Send)(struct _MODULE_BLUETOOTHHC06 *module,FuncFlag funcflag,rt_int16_t param);
    void (*Method_Feed)(struct _MODULE_BLUETOOTHHC06 *module);
    void (*Method_Code)(struct _MODULE_BLUETOOTHHC06 *module,FuncFlag funcflag,rt_int16_t param);
    void (*Method_Encode)(struct _MODULE_BLUETOOTHHC06 *module);
    rt_err_t (*Method_Handle)(rt_device_t dev, rt_size_t size);
    
};
typedef struct _MODULE_BLUETOOTHHC06 MODULE_BLUETOOTHHC06;

rt_err_t Module_BlueToothHC06_Config(MODULE_BLUETOOTHHC06 *module);

#endif

/************************ (C) COPYRIGHT 2020 WANGXI **************END OF FILE****/
