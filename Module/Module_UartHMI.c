/*
 * Copyright (c) 2020 - ~, HIT_HERO Team
 *
 * UART HMI MODULE SOUCE FILE
 * Used in RT-Thread Operate System
 *
 * Change Logs:
 * Date           Author       Notes                Mail
 * 2020-09-06     WangXi       first version        WangXi_chn@foxmail.com
 */
#include "Module_UartHMI.h"
#include <string.h>

static void Module_UartHMIInit(MODULE_UARTHMI *module);
static void Module_UartHMISend(MODULE_UARTHMI *module,char* string);
static void Module_UartHMIFeed(MODULE_UARTHMI *module,rt_uint8_t* packetID,rt_int32_t* param);
static rt_err_t Module_UartHMIHandle(rt_device_t dev, rt_size_t size);

static void UartHMIDecode(MODULE_UARTHMI *module,rt_uint8_t* packetID,rt_int32_t* param);

static struct rt_semaphore uartHMI_sem;

/* Global Method */
void Module_UartHMI_Config(MODULE_UARTHMI *module)
{
    if( module->Method_Init     == NULL &&
		module->Method_Send		== NULL &&
		module->Method_Feed		== NULL &&
		module->Method_Handle	== NULL
              
    ){  
        /* Link the Method */
        module->Method_Init         = Module_UartHMIInit;
		module->Method_Send			= Module_UartHMISend;
		module->Method_Feed			= Module_UartHMIFeed;
		module->Method_Handle		= Module_UartHMIHandle;
		
    }
    else{
        rt_kprintf("Warning: Module uart HMI is Configed twice\n");
        return;
    }

    /* Device Init */
    module->Method_Init(module);

    return;
}
 
static void Module_UartHMIInit(MODULE_UARTHMI *module)
{
	module->Value_uart_dev = rt_device_find(module->Property_UartDevName);
    if (!module->Value_uart_dev)
    {
        rt_kprintf("find %s failed!\t\n", module->Value_uart_dev);
        return;
    }
	
	rt_sem_init(&uartHMI_sem, "uartHMI_sem", 0, RT_IPC_FLAG_FIFO);
    
    struct serial_configure config = RT_SERIAL_CONFIG_DEFAULT;  /* 初始化配置参数 */
	
    /* step2：修改串口配置参数 */
	config.baud_rate = BAUD_RATE_9600;        //修改波特率为 9600
    config.data_bits = DATA_BITS_8;           //数据位 8
    config.stop_bits = STOP_BITS_1;           //停止位 1
    config.bufsz     = 128;                   //修改缓冲区 buff size 为 128
    config.parity    = PARITY_NONE;           //无奇偶校验位
    
    /* step3：控制串口设备。通过控制接口传入命令控制字，与控制参数 */
    rt_device_control(module->Value_uart_dev, RT_DEVICE_CTRL_CONFIG, &config);  
    rt_device_open(module->Value_uart_dev, RT_DEVICE_FLAG_DMA_RX);
	rt_device_set_rx_indicate(module->Value_uart_dev, module->Method_Handle);
	
	/* make sure communicate with HMI successfully */
	rt_thread_mdelay(200);
	rt_uint8_t endsign[3] = {0xff,0xff,0xff};
	rt_device_write(    module->Value_uart_dev, 0, 
                        endsign, 
                        sizeof(endsign));
	rt_thread_mdelay(200);
}
 
static void Module_UartHMISend(MODULE_UARTHMI *module,char* string)
{
	rt_device_write(    module->Value_uart_dev, 0, 
                        string, 
                        strlen(string));
	rt_thread_mdelay(1);
	rt_uint8_t endsign[3] = {0xff,0xff,0xff};
	rt_device_write(    module->Value_uart_dev, 0, 
                        endsign, 
                        sizeof(endsign));
}
 
static void Module_UartHMIFeed(MODULE_UARTHMI *module,rt_uint8_t* packetID,rt_int32_t* param)
{
    rt_uint8_t ch;
    rt_uint8_t i = 0;

    while (1)
    {   
        while (rt_device_read(module->Value_uart_dev, -1, &ch, 1) == 0)
        {
            rt_sem_control(&uartHMI_sem, RT_IPC_CMD_RESET, RT_NULL);
            rt_sem_take(&uartHMI_sem, RT_WAITING_FOREVER);
        }
		
        if((rt_uint8_t)ch == 0xAA)
        {
			/* get the frame end flag and start encode the frame */
			UartHMIDecode(module,packetID,param);
			
            i = 0;
            continue;
        }
        i = (i >= UARTHMI_FRAME_LENGTH-1) ? UARTHMI_FRAME_LENGTH-1 : i;
        module->Value_feedData[i++] = ch;
    }   
}

static rt_err_t Module_UartHMIHandle(rt_device_t dev, rt_size_t size)
{   
    rt_sem_release(&uartHMI_sem);

    return RT_EOK;
}

static void UartHMIDecode(MODULE_UARTHMI *module,rt_uint8_t* packetID,rt_int32_t* param)
{
    if(	(rt_uint8_t)(module->Value_feedData[0])==	0xA5 					&&
		(rt_uint8_t)(module->Value_feedData[1])==	0x5A 					&&
		(rt_uint8_t)(module->Value_feedData[2])==	module->Property_MyID 	&&
		(rt_uint8_t)((module->Value_feedData[2])+
		(module->Value_feedData[3])+(module->Value_feedData[4])+
		(module->Value_feedData[5])+(module->Value_feedData[6])+
		(module->Value_feedData[7])+(module->Value_feedData[8]))
		==(module->Value_feedData[9]))
    {
		//Split the data
		module->Value_GetID = module->Value_feedData[3];
		
		rt_int32_t Date = ((module->Value_feedData[5])<<24) + ((module->Value_feedData[6])<<16)
						+((module->Value_feedData[7])<<8) + ((module->Value_feedData[8]));	
		
		*packetID = module->Value_feedData[4];
		param[*packetID] = Date;
	}
    else
    {
        return;
    }
}
 
/************************ (C) COPYRIGHT 2020 WANGXI **************END OF FILE****/

