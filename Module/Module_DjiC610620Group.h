/*
 * Copyright (c) 2020 - ~, HIT_HERO Team
 *
 * 2508MOTOR_CAN MODULE HEAD FILE
 * Used in RT-Thread Operate System
 *
 * Change Logs:
 * Date           Author       Notes            	Mail
 * 2020-08-08     WangXi       first version    	WangXi_chn@foxmail.com
 *
 * Note:
 * DJI 3508 motor and C620 motor controler driver and control framework
 * All paramemters shall be subject to the motor rotor control
 * And the parameters of the shaft shall be converted by yourself if needed
 * The positive direction is clockwise facing the rotor
 */
 
#ifndef _MODULE_3508MOTORGROUP_H_
#define _MODULE_3508MOTORGROUP_H_

#include <rtthread.h>
#include <rtdevice.h>
#include <board.h>

#include "Math_PID.h"

/* Definition ----------------------------------------------------------*/
enum DjiC610620ID
{
	DjiC610620ID_1 = 0,
	DjiC610620ID_2 = 1,
	DjiC610620ID_3 = 2,
	DjiC610620ID_4 = 3,
	DjiC610620ID_5 = 4,
	DjiC610620ID_6 = 5,
	DjiC610620ID_7 = 6,
	DjiC610620ID_8 = 7,
};

enum DjiC610620MODE
{
	DjiC610620MODE_SPEED = 0,
	DjiC610620MODE_ANGLE = 1,
};

enum DjiC610620ENCODER
{
	DjiC610620SELF 	= 0,
	ROTARYENCODER 	= 1,
};

/* Single motor label ----------------------------------------------------------*/
struct _MODULE_DjiC610620
{
    /* Property */
	rt_uint8_t 		Enable;
	rt_uint8_t		Mode;
	
	MATH_PID 		PID_Speed;
	MATH_PID 		PID_Angle;
	
	enum DjiC610620ENCODER	ENCODER;
	
	//--------------------------------
	//if use extra Encoder must config 
	char * 			Encoder_DevName; 
	//--------------------------------
	
	/* Value */
	rt_device_t 	Encoder_dev;
	
	rt_int16_t Value_motor_AimCurrent;
	rt_int16_t Value_motor_AimRPM;
	rt_int16_t Value_motor_AimAngle;
	rt_int16_t Value_motor_RealCurrent;
	rt_int16_t Value_motor_RealRPM;
	rt_int16_t Value_motor_RealAngle;
	rt_uint8_t Value_motor_Temperature;
	
	rt_int8_t  Value_motor_InitFlag;
	rt_int16_t Value_motor_OffsetAngle;
	rt_int16_t Value_motor_LastAngle;
	rt_int32_t Value_motor_TotalAngle;   /* after the conversion (��) */
	rt_int16_t Value_motor_AngleCnt;
	
	/* Method */
    void (*Method_Init)(struct _MODULE_DjiC610620 *module);
};
typedef struct _MODULE_DjiC610620 MODULE_DjiC610620;


/* Motor group label ----------------------------------------------------------*/
struct _MODULE_DjiC610620GROUP
{
    /* Property */
	char * 				Property_CanDevName; 
	MODULE_DjiC610620 	Value_module_DjiC610620[7];
	
	/* Value */
	struct rt_can_msg 	Value_can_msgLow;
	struct rt_can_msg 	Value_can_msgHigh;
	struct rt_can_msg 	Value_can_mrg;
	rt_device_t 		Value_can_dev;
	rt_uint8_t 			Value_motorUsedMask;
	
	
	/* Method */
    void (*Method_Init)			(struct _MODULE_DjiC610620GROUP *module);
	void (*Method_Send)			(struct _MODULE_DjiC610620GROUP *module);
	void (*Method_Feed)			(struct _MODULE_DjiC610620GROUP *module);
	rt_err_t (*Method_Handle)	(rt_device_t dev,rt_size_t size);
	
};
typedef struct _MODULE_DjiC610620GROUP MODULE_DjiC610620GROUP;




/* Glodal Method */
rt_err_t Module_DjiC610620Group_Config(MODULE_DjiC610620GROUP *Dev_DjiC610620Group);






#endif
/************************ (C) COPYRIGHT 2020 WANGXI **************END OF FILE****/

