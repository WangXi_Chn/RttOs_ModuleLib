/*
 * Copyright (c) 2020 - ~, HIT_HERO Team
 *
 * ROBOMODULE MODULE HEAD FILE
 * Used in RT-Thread Operate System
 *
 * Change Logs:
 * Date           Author       Notes            	Mail
 * 2020-08-22     WangXi       first version    	WangXi_chn@foxmail.com
 * 
 * Note:
 * 
 */
 
#include <rtthread.h>
#include <rtdevice.h>
#include <board.h>

enum ROBOMODULE_GROUPID
{
	ROBOMODULE_GROUPID_0 = 0,
//	ROBOMODULE_GROUPID_1 = 1,
//	ROBOMODULE_GROUPID_2 = 2,
//	ROBOMODULE_GROUPID_3 = 3,
//	ROBOMODULE_GROUPID_4 = 4,
//	ROBOMODULE_GROUPID_5 = 5,
//	ROBOMODULE_GROUPID_6 = 6,
//	ROBOMODULE_GROUPID_7 = 7,
};

enum ROBOMODULE_NUMID
{
	ROBOMODULE_NUMID_1 		= 0,
	ROBOMODULE_NUMID_2 		= 1,
	ROBOMODULE_NUMID_3 		= 2,
	ROBOMODULE_NUMID_4 		= 3,
	ROBOMODULE_NUMID_5 		= 4,
	ROBOMODULE_NUMID_6 		= 5,
	ROBOMODULE_NUMID_7 		= 6,
	ROBOMODULE_NUMID_8 		= 7,
//	ROBOMODULE_NUMID_9 		= 9,
//	ROBOMODULE_NUMID_10 	= 10,
//	ROBOMODULE_NUMID_11 	= 11,
//	ROBOMODULE_NUMID_12 	= 12,
//	ROBOMODULE_NUMID_13 	= 13,
//	ROBOMODULE_NUMID_14 	= 14,
//	ROBOMODULE_NUMID_15 	= 15,
};

enum ROBOMODULE_MODE
{
	ROBOMODULE_MODE_OPENLOOP 			= 1,
	ROBOMODULE_MODE_CURRENT  			= 2,
	ROBOMODULE_MODE_SPEED  				= 3,
	ROBOMODULE_MODE_LOCATION			= 4,
	ROBOMODULE_MODE_SPEEDLOCATION		= 5,
	ROBOMODULE_MODE_CURRENTSPEED		= 6,
	ROBOMODULE_MODE_CURRENLOCATION		= 7,
	ROBOMODULE_MODE_CURRENSPEEDLOCATION	= 8,
};

/* Single module label ----------------------------------------------------------*/
struct _MODULE_ROBOMODULE
{
    /* Property */
	rt_uint8_t 					Property_Enable;
	enum ROBOMODULE_GROUPID		Property_GroupID;
	enum ROBOMODULE_NUMID		Property_NumID;
	enum ROBOMODULE_MODE		Property_Mode;
	
	/* Value */	
	rt_device_t 		Value_can_dev;
	struct rt_can_msg 	Value_can_msg;
	struct rt_can_msg 	Value_can_mrg;
	
	rt_int16_t Value_motor_AimCurrent;
	rt_int16_t Value_motor_AimRPM;
	rt_int32_t Value_motor_AimAngle;
	rt_int16_t Value_motor_RealCurrent;
	rt_int16_t Value_motor_RealRPM;
	rt_int32_t Value_motor_RealAngle;
	
	/* Method */
    void (*Method_Init)(struct _MODULE_ROBOMODULE *module);
};
typedef struct _MODULE_ROBOMODULE MODULE_ROBOMODULE;

/* Motor group label ----------------------------------------------------------*/
struct _MODULE_ROBOMODULEGROUP
{
    /* Property */
	char * 							Property_CanDevName; 
	
	/* Value */
	MODULE_ROBOMODULE 			Value_robomodule[8];	
	rt_device_t 		Value_can_dev;
	struct rt_can_msg 	Value_can_msg;
	struct rt_can_msg 	Value_can_mrg;
	
	rt_uint8_t 			Value_moduleUsedMask;
		
	/* Method */
    void (*Method_Init)			(struct _MODULE_ROBOMODULEGROUP *module);
	void (*Method_Send)			(struct _MODULE_ROBOMODULEGROUP *module);
	void (*Method_Feed)			(struct _MODULE_ROBOMODULEGROUP *module);
	rt_err_t (*Method_Handle)	(rt_device_t dev,rt_size_t size);
};
typedef struct _MODULE_ROBOMODULEGROUP MODULE_ROBOMODULEGROUP;


/* Glodal Method */
rt_err_t Module_RobomoduleGroup_Config(MODULE_ROBOMODULEGROUP *module);

