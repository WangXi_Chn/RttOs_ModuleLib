/*
 * Copyright (c) 2020 - ~, HIT_HERO Team
 *
 * UARTCOM MODULE HEAD FILE
 * Used in RT-Thread Operate System
 *
 * Change Logs:
 * Date           Author       Notes                Mail
 * 2020-08-25     WangXi       first version        WangXi_chn@foxmail.com
 * 2020-09-06     WangXi       second version       WangXi_chn@foxmail.com
 * Note
 */
#ifndef _MODULE_UARTCOM_H_
#define _MODULE_UARTCOM_H_

#include <rtthread.h>
#include <rtdevice.h>
#include <board.h>

#define UARTCOM_FRAME_LENGTH 11

typedef rt_uint8_t UARTCOM_MYID;
typedef rt_uint8_t UARTCOM_NETID;
typedef rt_uint8_t UARTCOM_AIMID;
typedef rt_uint8_t UARTCOM_GETID;
typedef rt_uint8_t UARTCOM_PACKETID;

typedef enum{
	SIMPLEUART = 0,
	JC24B,
	
}UARTCOM_DEVICE;

struct _MODULE_UARTCOM
{
    /* Property */
    char *              Property_UartDevName; 
    rt_uint32_t			Property_BaudRate;
	UARTCOM_MYID 		Property_MyID;
	UARTCOM_NETID		Property_NetID;
	UARTCOM_DEVICE		Property_Device;
	
	rt_base_t 			Property_JC24BsetPin;          /*!< Only used when Property_Device is JC24B as set pin. >
										This parameter is defined by function @ref GET_PIN(GPIOPORT, GPIO_PIN_NUM) */
	
    /* Value */
    rt_device_t         Value_uart_dev;
    rt_uint8_t          Value_sendData[UARTCOM_FRAME_LENGTH];
    rt_uint8_t          Value_feedData[UARTCOM_FRAME_LENGTH];
	UARTCOM_AIMID 		Value_AimID;
	UARTCOM_GETID 		Value_GetID;
    
    /* Method */
    void (*Method_Init)(struct _MODULE_UARTCOM *module);
    void (*Method_Send)(struct _MODULE_UARTCOM *module,UARTCOM_PACKETID packetID,rt_int32_t param);
    void (*Method_Feed)(struct _MODULE_UARTCOM *module,UARTCOM_PACKETID* packetID,rt_int32_t* param);
    rt_err_t (*Method_Handle)(rt_device_t dev, rt_size_t size);
    
};
typedef struct _MODULE_UARTCOM MODULE_UARTCOM;

rt_err_t Module_UartCom_Config(MODULE_UARTCOM *module);

#endif

/************************ (C) COPYRIGHT 2020 WANGXI **************END OF FILE****/


