/*
 * Copyright (c) 2020 - ~, HIT_HERO Team
 *
 * KEY MODULE HEAD FILE
 * Used in RT-Thread Operate System
 *
 * Change Logs:
 * Date           Author       Notes            	Mail
 * 2020-08-07     WangXi       first version    	WangXi_chn@foxmail.com
 */
#ifndef _MODULE_KEY_H_
#define _MODULE_KEY_H_

#include <rtthread.h>
#include <rtdevice.h>
#include <board.h>

struct _MODULE_KEY
{
    /* Property */
    rt_base_t pin;          /*!< Specifies the LED module pins to be configured.
                            This parameter is defined by function @ref GET_PIN(GPIOPORT, GPIO_PIN_NUM) */
  
	/* Value */
	rt_int32_t status;
	
	/* Method */
    void (*Init)(struct _MODULE_KEY *module);
	void (*Update)(struct _MODULE_KEY *module);
	void (*Handle)(void *args);

};
typedef struct _MODULE_KEY MODULE_KEY;

/* Glodal Method */
rt_err_t Module_Key_Config(MODULE_KEY *Dev_key);

#endif 
/************************ (C) COPYRIGHT 2020 WANGXI **************END OF FILE****/

