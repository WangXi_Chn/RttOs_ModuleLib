/*
 * Copyright (c) 2020 - ~, HIT_HERO Team
 *
 * AS5048 MODULE HEAD FILE
 * Used in RT-Thread Operate System
 *
 * Change Logs:
 * Date           Author       Notes			QQ
 * 2020-06-13     WangXi   	   first version	1098547591
 */
 
#ifndef _MODULE_AS5048_
#define _MODULE_AS5048_

#include <rtthread.h>
#include <rtdevice.h>
#include <board.h>

// 附加偶校验位的各个寄存器读取指令
#define AS5048A_CMD_READ_ANGLE    ((1<<15) | (1<<14) | 0x3FFF)
#define AS5048A_CMD_READ_MAG      ((0<<15) | (1<<14) | 0x3FFE)
#define AS5048A_CMD_READ_DIAG     ((0<<15) | (1<<14) | 0x3FFD)
#define AS5048A_CMD_NOP           ((0<<15) | (0<<14) | 0x0000)
#define AS5048A_CMD_CLEAR_ERROR   ((0<<15) | (1<<14) | 0x0001)

struct _MODULE_AS5048
{
	/* Property */
	char *SpiBusName;			 
	char *SpiDevName;
	
	GPIO_TypeDef *CsPort;
	rt_uint16_t CsPinNum;
	
	rt_uint16_t TurnRadius;
	
	/* Value */
	struct rt_spi_device *SPI_DevHandle;
	
	rt_uint8_t error_flag;
	
	rt_int16_t Orignal_Date;
	rt_int16_t Offset_Data;
    
    rt_int16_t ECD_Data;           //单圈数据
    rt_int16_t ECD_Data_LAST;
    
    rt_int32_t ECD_TotalData;      //整体角度
	
	rt_int32_t ECD_RoundCnt;

    float ECD_Distance;	

	/* Method */
	rt_err_t (*Init)(struct _MODULE_AS5048 *module);
	void (*GetOrignalData)(struct _MODULE_AS5048 *module);
	void (*GetOffsetData)(struct _MODULE_AS5048 *module);
	void (*GetDistance)(struct _MODULE_AS5048 *module);
};
typedef struct _MODULE_AS5048 MODULE_AS5048;


/* Glodal Method */
rt_err_t Module_AS5048_Config(MODULE_AS5048 *Dev_AS5048);





#endif

/************************ (C) COPYRIGHT 2020 WANGXI **************END OF FILE****/
