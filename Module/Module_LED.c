/*
 * Copyright (c) 2020 - ~, HIT_HERO Team
 *
 * LED MODULE SOUCE FILE
 * Used in RT-Thread Operate System
 *
 * Change Logs:
 * Date           Author       Notes            	Mail
 * 2020-06-02     WangXi       first version    	WangXi_chn@foxmail.com
 * 2020-07-30     WangXi       second version    	WangXi_chn@foxmail.com
 */
 
#include "Module_LED.h"
#include <stdlib.h>

/* User Finsh Code Begin*/



/* User Finsh Code End */

/* Static Method */
static void Module_LedInit(MODULE_LED *module);
static void Module_LedSet(MODULE_LED *module,rt_uint8_t number);
static void Module_LedHandle(MODULE_LED *module);

/* Global Method */
rt_err_t Module_Led_Config(MODULE_LED *Dev_LED){
    if(Dev_LED->Method_Init==NULL &&
		Dev_LED->Method_Set==NULL &&
		Dev_LED->Method_Handle==NULL

    ){  
        /* Link the Method */
        Dev_LED->Method_Init = Module_LedInit;
		Dev_LED->Method_Set = Module_LedSet;
		Dev_LED->Method_Handle = Module_LedHandle;
    }
    else{
        rt_kprintf("Warning: Module Led is Configed twice\n");
        return RT_ERROR;
    }

    /* Device Init */
    Dev_LED->Method_Init(Dev_LED);
    return RT_EOK;
}

/* Static Method */
static void Module_LedInit(MODULE_LED *module){
    rt_pin_mode(module->Property_pin, PIN_MODE_OUTPUT);
	module->set_time_cycle = (rt_uint16_t)-1;
	module->curr_number = 0;
	if(module->Property_Mode == SIMPLE_LED_MODE)
	{
		rt_pin_write(module->Property_pin, MODULE_LED_OFF);
	}
	
}

static void Module_LedSet(MODULE_LED *module,rt_uint8_t number){
	if(module->Property_Mode == FLASH_LED_MODE)
	{
		module->next_number = number;
	}
    else if(module->Property_Mode == SIMPLE_LED_MODE)
	{
		rt_pin_write(module->Property_pin, number);
	}

}

static void Module_LedHandle(MODULE_LED *module){
	/* get system clock */
	rt_uint32_t time = rt_tick_get();
	
	/* toggle the LED */
    if(((uint16_t)(time - module->set_last_time)) >= module->set_time_cycle) {
        module->set_last_time = time;
        if(module->curr_number) {
            /* enable LED pin */
			rt_pin_write(module->Property_pin, ((module->curr_number & 1) == 1) ? MODULE_LED_OFF : MODULE_LED_ON);
            module->curr_number--;
            if(module->curr_number == 0) {
                module->set_time_cycle = (uint16_t)-1;// 下一个周期不再进入
            }
        }
    }
	/* enable the new param */
	if(((uint16_t)(time - module->last_time_show_cycle)) >= module->LED_TIME_CYCLE){
        module->last_time_show_cycle = time;

        if(!module->curr_number){
			uint8_t number = module->next_number; // 获取当前指示次数
			// 限制闪烁次数
			if(number < (module->LED_TIME_CYCLE / module->LED_TIME_OUTPUT / 2)){
				module->curr_number    = number * 2 - 1;
				module->set_last_time  = time;
				module->set_time_cycle = module->LED_TIME_OUTPUT;
				/* enable LED pin */
				rt_pin_write(module->Property_pin, MODULE_LED_ON);
			}
        }
    }
    
}



/************************ (C) COPYRIGHT 2020 WANGXI **************END OF FILE****/

