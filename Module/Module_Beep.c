/*
 * Copyright (c) 2020 - ~, HIT_HERO Team
 *
 * BEEP MODULE SOUCE FILE
 * Used in RT-Thread Operate System
 *
 * Change Logs:
 * Date           Author       Notes            	Mail
 * 2020-07-31     WangXi       second version    	WangXi_chn@foxmail.com
 */
 
#include "Module_Beep.h"
#include <stdlib.h>

/* User Code Begin*/
static void Beep_shine_entry(void *parameter){
	MODULE_BEEP* dev_beep_1 = (MODULE_BEEP *)parameter;
	dev_beep_1->Loop(dev_beep_1);
}

void BeepTest(int argc,char **argv)
{
    rt_err_t res = RT_EOK;
    
    MODULE_BEEP dev_beep_1 = {"pwm1",4,2000,20,1500,15};
    res = Module_Beep_Config(&dev_beep_1);
    if(res != RT_EOK){
        rt_kprintf("Error: Some wrong happened while BEEP config\n");
        return;
    }
    
    if (argc < 2){
        rt_kprintf("Error: Command missing arguments\n");
        return;
    }
    
	rt_uint32_t num = atoi(argv[1]); 
	dev_beep_1.Set(&dev_beep_1,num);
	
	
	rt_thread_t BEEP_thread = rt_thread_create("beepshine", Beep_shine_entry, &dev_beep_1,
                                  192, RT_THREAD_PRIORITY_MAX - 2, 20);
    if (BEEP_thread != RT_NULL){
        rt_thread_startup(BEEP_thread);
    }
}
MSH_CMD_EXPORT(BeepTest , BeepTest(PH11) <-number>(1~4));
/* User Code End */

/* Static Method */
static void Module_BeepInit(MODULE_BEEP *module);
static void Module_BeepSet(MODULE_BEEP *module,rt_uint8_t number);
static void Module_BeepLoop(MODULE_BEEP *module);
static void Module_BeepControl(MODULE_BEEP *module,BEEP_STATION station);

/* Global Method */
rt_err_t Module_Beep_Config(MODULE_BEEP *Dev_Beep){
    if( Dev_Beep->Init==NULL &&
		Dev_Beep->Set==NULL &&
		Dev_Beep->Loop==NULL &&
		Dev_Beep->Control==NULL

    ){  
        /* Link the Method */
        Dev_Beep->Init = Module_BeepInit;
		Dev_Beep->Set = Module_BeepSet;
		Dev_Beep->Loop = Module_BeepLoop;
		Dev_Beep->Control = Module_BeepControl;
    }
    else{
        rt_kprintf("Warning: Module BEEP is Configed twice\n");
        return RT_ERROR;
    }

    /* Device Init */
    Dev_Beep->Init(Dev_Beep);
    return RT_EOK;
}

/* Static Method */
static void Module_BeepInit(MODULE_BEEP *module){	
	rt_uint32_t PWM_period = 1000000000/module->PWM_FREQ;
    rt_uint32_t PWM_pulse = PWM_period/100*module->PWM_DUTY; 
	module->pwm_dev = (struct rt_device_pwm *)rt_device_find(module->PwmDevName);
	
	if (module->pwm_dev == RT_NULL)
        rt_kprintf("beep init failed! can't find pwm_beep device!\n");
    
	rt_pwm_set(module->pwm_dev, module->PWM_CHANNEL, PWM_period, PWM_pulse);
	
	module->set_time_cycle = (rt_uint16_t)-1;
	module->curr_number = 0;
	
}

static void Module_BeepSet(MODULE_BEEP *module,rt_uint8_t number){
    module->next_number = number;
    
}

static void Module_BeepLoop(MODULE_BEEP *module){
	/* get system clock */
	rt_uint32_t time = rt_tick_get();
	
	/* toggle the BEEP */
    if(((uint16_t)(time - module->set_last_time)) >= module->set_time_cycle) {
        module->set_last_time = time;
        if(module->curr_number) {
            
			//rt_pin_write(module->pin, ((module->curr_number & 1) == 1) ? MODULE_BEEP_OFF : MODULE_BEEP_ON);
            module->Control(module,((module->curr_number & 1) == 1) ? MODULE_BEEP_OFF : MODULE_BEEP_ON);
			module->curr_number--;
            if(module->curr_number == 0) {
                module->set_time_cycle = (uint16_t)-1;// 下一个周期不再进入
            }
        }
    }
	/* enable the new param */
	if(((uint16_t)(time - module->last_time_show_cycle)) >= module->BEEP_TIME_CYCLE){
        module->last_time_show_cycle = time;

        if(!module->curr_number){
			uint8_t number = module->next_number; // 获取当前指示次数
			// 限制闪烁次数
			if(number < (module->BEEP_TIME_CYCLE / module->BEEP_TIME_OUTPUT / 2)){
				module->curr_number    = number * 2 - 1;
				module->set_last_time  = time;
				module->set_time_cycle = module->BEEP_TIME_OUTPUT;				
				module->Control(module,MODULE_BEEP_ON);				
			}
        }
    }
    
}

static void Module_BeepControl(MODULE_BEEP *module,BEEP_STATION station){
	switch(station){
		case MODULE_BEEP_ON:{
			rt_pwm_enable(module->pwm_dev, module->PWM_CHANNEL);
			break;
			}
		case MODULE_BEEP_OFF:{
			rt_pwm_disable(module->pwm_dev, module->PWM_CHANNEL);
			break;
			}
		}
}

/************************ (C) COPYRIGHT 2020 WANGXI **************END OF FILE****/

