/*
 * Copyright (c) 2020 - ~, HIT_HERO Team
 *
 * LED MODULE HEAD FILE
 * Used in RT-Thread Operate System
 *
 * Change Logs:
 * Date           Author       Notes            	Mail
 * 2020-06-02     WangXi       first version    	WangXi_chn@foxmail.com
 * 2020-07-30     WangXi       second version    	WangXi_chn@foxmail.com
 */
#ifndef _MODULE_LED_H_
#define _MODULE_LED_H_

#define _LED_POSITIVE_POLE      //RESET Enable LED
//#define _LED_NEGETIVE_POLE    //SET Enable LED

#include <rtthread.h>
#include <rtdevice.h>
#include <board.h>

enum LED_LEVEL
{
#ifdef _LED_POSITIVE_POLE
    MODULE_LED_ON = 0,
    MODULE_LED_OFF
#endif

#ifdef _LED_NEGETIVE_POLE
    MODULE_LED_OFF = 0,
    MODULE_LED_ON
#endif  
};

enum LED_MODE
{
	SIMPLE_LED_MODE = 0,
	FLASH_LED_MODE
};

struct _MODULE_LED
{
    /* Property */
    rt_base_t Property_pin;          /*!< Specifies the LED module pins to be configured.
                            This parameter is defined by function @ref GET_PIN(GPIOPORT, GPIO_PIN_NUM) */

	enum LED_MODE Property_Mode;
	 
	rt_uint32_t LED_TIME_CYCLE;     /* if Property_Mode is FLASH_LED_MODE, must be defined*/
	rt_uint32_t LED_TIME_OUTPUT;  

	/* Value */
	rt_uint16_t last_time_show_cycle;  	// 状态查询时间
    rt_uint16_t set_time_cycle;        	// 循环时间
    rt_uint16_t set_last_time;         	// 上一次电平输出时间
    rt_uint8_t  curr_number;        	// 当前
    rt_uint8_t  next_number;           	// 下一个指示次数
	
	/* Method */
    void (*Method_Init)(struct _MODULE_LED *module);
	void (*Method_Handle)(struct _MODULE_LED *module);
	void (*Method_Set)(struct _MODULE_LED *module,rt_uint8_t number);

};
typedef struct _MODULE_LED MODULE_LED;

/* Glodal Method */
rt_err_t Module_Led_Config(MODULE_LED *Dev_LED);

#endif 
/************************ (C) COPYRIGHT 2020 WANGXI **************END OF FILE****/

