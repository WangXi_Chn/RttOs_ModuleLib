/*
 * Copyright (c) 2020 - ~, HIT_HERO Team
 *
 * AS5048 MODULE SOURCE FILE
 * Used in RT-Thread Operate System
 *
 * Change Logs:
 * Date           Author       Notes			Mail
 * 2020-06-13     WangXi   	   first version	WangXi_Chn@foxmail.com
 */

#include "Module_AS5048.h"
#include "drv_spi.h"
#include <math.h>

/* User Code Begin*/
void AS5Test(int argc,char **argv)
{
	rt_err_t res = RT_EOK;
	
	/* Get orignal data command */
	if (argc < 2){
        rt_kprintf("Error: date num can't be NULL\r\n");
        return;
    }
	
	MODULE_AS5048 dev_as5048a = {"spi2","spi20",GPIOB,GPIO_PIN_12,48};
	res = Module_AS5048_Config(&dev_as5048a);
	if(res != RT_EOK){
		rt_kprintf("Error: Some wrong happened while mpu config\r\n");
		return;
	}
	
	rt_uint32_t count = atoi(argv[1]); 
	for(rt_uint32_t i = 0;i < count;){
		dev_as5048a.GetOrignalData(&dev_as5048a);
		rt_kprintf("Info: %d\n",dev_as5048a.Orignal_Date);
		
		/* spi communication test */
		
		struct rt_spi_message msg;
		rt_uint16_t sendbuf = 0;
		rt_uint16_t recvbuf = 0;

		msg.send_buf   = &sendbuf;
		msg.recv_buf   = &recvbuf;
		msg.length     = 2;
		msg.cs_take    = 0;
		msg.cs_release = 1;
		msg.next       = RT_NULL;
	
		sendbuf = AS5048A_CMD_READ_DIAG;
		rt_spi_transfer_message(dev_as5048a.SPI_DevHandle, &msg);
		rt_kprintf("info:send %x\r\n",sendbuf);
		
		//rt_thread_mdelay(100);
	}
	
}
MSH_CMD_EXPORT(AS5Test , AS5Test(spi) <date num>);


/* User Code End */
static rt_err_t Module_AS5048Init(MODULE_AS5048* module);
static void Module_GetOringeData(MODULE_AS5048* module);
static void Module_GetOffsetData(MODULE_AS5048* module);
static void Module_GetDistance(MODULE_AS5048* module);

/* Global Method */
rt_err_t Module_AS5048_Config(MODULE_AS5048 *Dev_AS5048){
	if(	Dev_AS5048->Init			==NULL && 
		Dev_AS5048->GetOrignalData	==NULL && 
		Dev_AS5048->GetOffsetData	==NULL &&
		Dev_AS5048->GetDistance     ==NULL
	){	
		/* Link the Method */
		Dev_AS5048->Init			= Module_AS5048Init;
		Dev_AS5048->GetOrignalData	= Module_GetOringeData;
		Dev_AS5048->GetOffsetData	= Module_GetOffsetData;	
		Dev_AS5048->GetDistance     = Module_GetDistance;
	}
	else{
		rt_kprintf("Warning: Module AS5048 is Configed twice\n");
		return RT_ERROR;
	}

	/* Device Init */
	Dev_AS5048->Init(Dev_AS5048);
	return RT_EOK;
}

/* Private Method */
static rt_err_t Module_AS5048Init(MODULE_AS5048* Dev_AS5048){
	rt_err_t res = RT_EOK;
	
	/* Init the spi communication */
	res = rt_hw_spi_device_attach(	Dev_AS5048->SpiBusName, 	Dev_AS5048->SpiDevName, 
									Dev_AS5048->CsPort, 		Dev_AS5048->CsPinNum);
	
	if(res != RT_EOK){
		rt_kprintf("Error: Spi device attach failed\r\n");
		return RT_ERROR;
	}
	
	Dev_AS5048->SPI_DevHandle = (struct rt_spi_device *)rt_device_find(Dev_AS5048->SpiDevName);
	if(Dev_AS5048->SPI_DevHandle == NULL){
		rt_kprintf("Error: Can't find %s device\r\n",Dev_AS5048->SpiDevName);
		return RT_ERROR;
	}
	
	struct rt_spi_configuration cfg;
    cfg.data_width = 16;
    cfg.mode = RT_SPI_MASTER | RT_SPI_MODE_1 | RT_SPI_MSB;
    cfg.max_hz = 5 * 1000 *1000;                           /* 1M */
    rt_spi_configure(Dev_AS5048->SPI_DevHandle, &cfg);
	
	return RT_EOK;
}

// 奇偶校验
static uint8_t parity_even(uint16_t v) 
{
    if(v == 0) return 0;

    v ^= v >> 8;
    v ^= v >> 4;
    v ^= v >> 2;
    v ^= v >> 1;
    
    return v & 1;
}

static void Module_GetOringeData(MODULE_AS5048* Dev_AS5048){
	uint16_t data = 0;
	Dev_AS5048->error_flag = 1;
	
    rt_uint16_t sendbuf = 0;
	rt_uint16_t recvbuf = 0;
	
	sendbuf = AS5048A_CMD_READ_ANGLE;
	rt_size_t size = rt_spi_transfer(Dev_AS5048->SPI_DevHandle,&sendbuf,&recvbuf,1);
	if(size == 0)
	{
		rt_kprintf("error: spi transfer failed!\r\n");
	}
	
	sendbuf = AS5048A_CMD_NOP;
	size = rt_spi_transfer(Dev_AS5048->SPI_DevHandle,&sendbuf,&recvbuf,1);
	if(size == 0)
	{
		rt_kprintf("error: spi transfer failed!\r\n");
	}
	
    if ((recvbuf & (1 << 14)) == 0) {
        data = (recvbuf & 0x3FFF);
        Dev_AS5048->error_flag = (parity_even(data) ^ (recvbuf >> 15));
    } 
    else {
		sendbuf = AS5048A_CMD_CLEAR_ERROR;
        rt_spi_transfer(Dev_AS5048->SPI_DevHandle,&sendbuf,&recvbuf,1);
    }
	
	if(Dev_AS5048->error_flag){
		data = 0;	
	}
	
	//if the data can not be used
	if(data==0)
		return;
	
	Dev_AS5048->Orignal_Date = data;
}

static void Module_GetOffsetData(MODULE_AS5048* Dev_AS5048){
	rt_int32_t OriginalDateSum = 0;
	rt_int8_t Count = 0;
	for(rt_int8_t i = 0;i<20;i++){
		Dev_AS5048->GetOrignalData(Dev_AS5048);
		if(Dev_AS5048->Orignal_Date != 0)
		{
			OriginalDateSum += Dev_AS5048->Orignal_Date;
			Count++;
		}		
	}
	Dev_AS5048->Offset_Data = OriginalDateSum/Count;
	Dev_AS5048->ECD_Data_LAST = Dev_AS5048->Offset_Data;
}

static void Module_GetDistance(MODULE_AS5048* Dev_AS5048){  
	Dev_AS5048->GetOrignalData(Dev_AS5048);
    Dev_AS5048->ECD_Data = Dev_AS5048->Orignal_Date;    			//一圈360°实际对应16384
    
    if(Dev_AS5048->ECD_Data>16384)                            		//数据合法检查
		rt_kprintf("Warning: get illigal data!");
	
    if(Dev_AS5048->ECD_Data - Dev_AS5048->ECD_Data_LAST > 8192)		//多圈计数
        Dev_AS5048->ECD_RoundCnt --;
	
    if(Dev_AS5048->ECD_Data - Dev_AS5048->ECD_Data_LAST < -8192)
        Dev_AS5048->ECD_RoundCnt ++;
    
    Dev_AS5048->ECD_TotalData = Dev_AS5048->ECD_RoundCnt *16384 + Dev_AS5048->ECD_Data - Dev_AS5048->Offset_Data;//获取整体数据
    Dev_AS5048->ECD_Distance = Dev_AS5048->ECD_TotalData *2 *3.1415 * Dev_AS5048->TurnRadius /16384;
	Dev_AS5048->ECD_Data_LAST = Dev_AS5048->ECD_Data;
}


/************************ (C) COPYRIGHT 2020 WANGXI **************END OF FILE****/

