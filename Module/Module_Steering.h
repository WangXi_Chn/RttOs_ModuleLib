/*
 * Copyright (c) 2020 - ~, HIT_HERO Team
 *
 * X-SPIDER APP HEAD FILE
 * Used in RT-Thread Operate System
 *
 * Change Logs:
 * Date           Author       Notes                Mail
 * 2020-12-16     WangXi       first version        WangXi_chn@foxmail.com
 */
 
#ifndef __MODULE_STEERING_H__
#define __MODULE_STEERING_H__
 
#include <rtthread.h>
#include <rtdevice.h>
#include <board.h>

enum TYPE_STEERING
{
	SG90 = 0,
	TYPE_STEERING_END
};
 
struct _MODULE_STEERING
{
	/* Property */
	enum TYPE_STEERING 	Property_SteerType;
    char * 				Property_PwmDevName;
	rt_uint8_t 			Property_PWM_CHANNEL;
	
	rt_uint32_t Property_PulseMINus;
    rt_uint32_t Property_PulseMAXus;
	rt_int16_t  Property_AngleMAX;
	rt_int16_t  Property_InitAngle;


	/* Value */
    rt_int16_t  Value_Angle;
    rt_uint32_t Value_PWM_PERIOD_NS;
    rt_uint32_t Value_PWM_PULSE_NS;
	
    struct rt_device_pwm *pwm_dev;
	
	
	/* Method */
    void (*Method_Init)(struct _MODULE_STEERING *module);
	void (*Method_Enable)(struct _MODULE_STEERING *module);
	void (*Method_Disable)(struct _MODULE_STEERING *module);
	void (*Method_SetPulse)(struct _MODULE_STEERING *module,rt_uint32_t pwm_pulse_ns);
	void (*Method_SetAngle)(struct _MODULE_STEERING *module,rt_int16_t angle);
};
typedef struct _MODULE_STEERING MODULE_STEERING;

/* Glodal Method */
rt_err_t Module_Steering_Config(MODULE_STEERING *Dev_Steering);

 
#endif
 /************************ (C) COPYRIGHT 2020 WANGXI **************END OF FILE****/
