/*
 * Copyright (c) 2020 - ~, HIT_HERO Team
 *
 * FILE MODULE HEAD FILE
 * Used file system
 * Used in RT-Thread Operate System
 *
 * Change Logs:
 * Date           Author       Notes			QQ
 * 2020-06-04     WangXi   	   first version	1098547591
 */
 
#ifndef _MODULE_FILE_H_
#define _MODULE_FILE_H_

#include <rtthread.h>
#include <rtdevice.h>
#include <board.h>

struct _MODULE_FILE
{
	/* Property */
	char *SpiBusName;			 
	char *SpiDevName;
	char *BlockDevName;
	
	rt_uint32_t ChipIDRegist;	
	rt_uint32_t BlockChipID;	
	
	GPIO_TypeDef *CsPort;
	rt_uint16_t CsPinNum;
	rt_bool_t IfFormatting;
	
	/* Value */
	struct rt_spi_device *SPI_DevHandle;
		

	/* Method */
	rt_err_t (*Init)(struct _MODULE_FILE *module);

};
typedef struct _MODULE_FILE MODULE_FILE;


/* Glodal Method */
rt_err_t Module_File_Config(MODULE_FILE *Dev_LED);




#endif

/************************ (C) COPYRIGHT 2020 WANGXI **************END OF FILE****/
