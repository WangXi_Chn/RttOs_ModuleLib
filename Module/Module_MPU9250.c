/*
 * Copyright (c) 2020 - ~, HIT_HERO Team
 *
 * MPU9250 MODULE SOURCE FILE
 * Used in RT-Thread Operate System
 *
 * Change Logs:
 * Date           Author       Notes			Mail
 * 2020-09-04     WangXi   	   second version	WangXi_Chn@foxmail.com
 */

#include "Module_MPU9250.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/* User Code End */
static rt_err_t Module_MPU9250Init(MODULE_MPU9250* module);
static void Module_GetTemperature(MODULE_MPU9250* module);
static void Module_GetOringalGyro(MODULE_MPU9250* module);
static void Module_GetOringalAccel(MODULE_MPU9250* module);
static void Module_GetOringalMag(MODULE_MPU9250* module);
static void Module_GetOffsetGyro(MODULE_MPU9250* module);
static void Module_GetAngleGyro(MODULE_MPU9250* module);

static void MPU_Write_Byte(rt_uint8_t addr,rt_uint8_t reg,rt_uint8_t data,MODULE_MPU9250* module);
static rt_uint8_t MPU_Read_Byte(rt_uint8_t addr,rt_uint8_t reg,MODULE_MPU9250* module);
static void MPU_Read_Len(rt_uint8_t addr,rt_uint8_t reg,rt_uint8_t len,rt_uint8_t *buf,MODULE_MPU9250* module);
static void MPU_Set_Rate(rt_uint16_t rate,MODULE_MPU9250* module);
static void MPU_Set_LPF(rt_uint16_t lpf,MODULE_MPU9250* module);
static void MPU_Set_Accel_Fsr(rt_uint8_t fsr,MODULE_MPU9250* module);
static void MPU_Set_Gyro_Fsr(rt_uint8_t fsr,MODULE_MPU9250* module);

/* Global Method */
rt_err_t Module_MPU9250_Config(MODULE_MPU9250 *Dev_MPU9250){
	
	rt_err_t res = RT_EOK;
	
	if(	Dev_MPU9250->Init				== NULL && 
		Dev_MPU9250->GetOringalAccel	== NULL && 
		Dev_MPU9250->GetOringalGyro		== NULL && 
		Dev_MPU9250->GetOringalMag 		== NULL &&
		Dev_MPU9250->GetChipTemperature	== NULL &&
		Dev_MPU9250->GetOffsetGyro 		== NULL &&
		Dev_MPU9250->GetAngleGyro 		== NULL
	){	
		/* Link the Method */
		Dev_MPU9250->Init 				= Module_MPU9250Init;
		Dev_MPU9250->GetOringalAccel 	= Module_GetOringalAccel;
		Dev_MPU9250->GetOringalGyro 	= Module_GetOringalGyro;
		Dev_MPU9250->GetOringalMag 		= Module_GetOringalMag;
		Dev_MPU9250->GetChipTemperature = Module_GetTemperature;
		Dev_MPU9250->GetOffsetGyro 		= Module_GetOffsetGyro;
		Dev_MPU9250->GetAngleGyro 		= Module_GetAngleGyro;
	}
	else{
		rt_kprintf("Warning: Module MPU9250 is Configed twice\r\n");
		return RT_ERROR;
	}

	/* Device Init */
	res = Dev_MPU9250->Init(Dev_MPU9250);
	if(res != RT_EOK){
		rt_kprintf("Error: MPU9250 init failed\r\n");
		return RT_ERROR;
	}
	
	Module_KalmanFilter_Config(&(Dev_MPU9250->GyroZaxisFilter));
	
	return RT_EOK;
}
/* Private Method */
static rt_err_t Module_MPU9250Init(MODULE_MPU9250* module)
{
	module->I2C_DevHandle = rt_i2c_bus_device_find(module->I2CbusName);
	
	if (module->I2C_DevHandle == RT_NULL) {
        rt_kprintf("Error: Can't find %s device\r\n",module->I2CbusName);
        return RT_ERROR;
    }
	
    rt_uint8_t ID=0;
	
	ID=MPU_Read_Byte(MPU9250_ADDR,MPU_DEVICE_ID_REG,module);  		//读取MPU6500的ID
	//rt_kprintf("Information: MPU9250 Gyroscope MPU6500's ID is %d\r\n",ID);
    if(ID==MPU6500_ID) {	
		MPU_Write_Byte(MPU9250_ADDR,MPU_PWR_MGMT1_REG,0X80,module);	//复位MPU9250
		rt_thread_mdelay(100);  
		MPU_Write_Byte(MPU9250_ADDR,MPU_PWR_MGMT1_REG,0X00,module);	//唤醒MPU9250
		MPU_Set_Gyro_Fsr(_250DPS,module);				
		MPU_Set_Accel_Fsr(_2G,module);					       	 	
		MPU_Set_Rate(50,module);						       	 	
		MPU_Write_Byte(MPU9250_ADDR,MPU_INT_EN_REG,0X00,module);   	//关闭所有中断
		MPU_Write_Byte(MPU9250_ADDR,MPU_USER_CTRL_REG,0X00,module);	//I2C主模式关闭
		MPU_Write_Byte(MPU9250_ADDR,MPU_FIFO_EN_REG,0X00,module);	//关闭FIFO
		MPU_Write_Byte(MPU9250_ADDR,MPU_INTBP_CFG_REG,0X82,module);	//INT引脚低电平有效，开启bypass模式，可以直接读取磁力计
        MPU_Write_Byte(MPU9250_ADDR,MPU_PWR_MGMT1_REG,0X01,module);	//设置CLKSEL,PLL X轴为参考
        MPU_Write_Byte(MPU9250_ADDR,MPU_PWR_MGMT2_REG,0X00,module);	//加速度与陀螺仪都工作
		MPU_Set_Rate(1000,module);									//设置采样率为1000Hz
		rt_thread_mdelay(100); 
    }
	else{
		rt_kprintf("Error: Can't find MPU9250 Gyroscope MPU6500\r\n");
	}
 
    ID=MPU_Read_Byte(AK8963_ADDR,MAG_WIA,module);    				//读取AK8963 ID
	//rt_kprintf("Information: MPU9250 Magnetometer AK8963's ID is %d\r\n",ID);	
    if(ID==AK8963_ID){
        MPU_Write_Byte(AK8963_ADDR,MAG_CNTL1,0X11,module);			//设置AK8963为单次测量模式
    }
	else{
		rt_kprintf("Error: Can't find MPU9250 Magnetometer AK8963\r\n");
        return RT_ERROR;
	}

    return RT_EOK;
}

static void Module_GetOffsetGyro(MODULE_MPU9250* module){
	float result = 0;
	for(rt_int32_t i = 0;i<module->OffsetCulCount;i++){
		rt_thread_mdelay(1);
		module->GetOringalGyro(module);
		result += module->OringalGyroscope[Z_AXIS];
	}
	module->OffsetGyroscope[Z_AXIS] = result/(module->OffsetCulCount);
}

static void Module_GetAngleGyro(MODULE_MPU9250* module){
	module->tick_now = rt_tick_get();
	rt_tick_t tick_delta = module->tick_now - module->tick_last;
	
	/* Get origin data */
	module->GetOringalGyro(module);
	
	/* unit conversion 32767/250 = 131.068 */
	float real_OringalGyroZaxis = (float)(((module->OringalGyroscope[Z_AXIS]) - (module->OffsetGyroscope[Z_AXIS]))/131.068);
	
	/* Use the Kalmanfileter */
	module->GyroZaxisFilter.Method_Update(&(module->GyroZaxisFilter),real_OringalGyroZaxis);
	real_OringalGyroZaxis = module->GyroZaxisFilter.Value_out;
	
	module->AngleGyroscope[Z_AXIS] -= (real_OringalGyroZaxis * tick_delta * 0.001f);
	module->AngleGyroscope[Z_AXIS] = fmod(module->AngleGyroscope[Z_AXIS],360.0f);
	
	//negetive angle changed to positive angle
	if(module->AngleGyroscope[Z_AXIS] < 0){
		module->AngleGyroscope[Z_AXIS] += 360.0f;
	}
	
	module->tick_last = module->tick_now;
	
	if(module->SWICH_DEBUG == 1)
	{
		char stringBuff[50];
		sprintf(stringBuff,"info: GetAngGyro_z %lf\r\n",module->AngleGyroscope[Z_AXIS]);
		rt_kprintf(stringBuff);
	}
}

//设置MPU9250陀螺仪传感器满量程范围
//fsr:0,±250dps;1,±500dps;2,±1000dps;3,±2000dps
static void  MPU_Set_Gyro_Fsr(rt_uint8_t fsr,MODULE_MPU9250* module){
	MPU_Write_Byte(MPU9250_ADDR,MPU_GYRO_CFG_REG,fsr<<3,module);
}

//设置MPU9250加速度传感器满量程范围
//fsr:0,±2g;1,±4g;2,±8g;3,±16g
static void MPU_Set_Accel_Fsr(rt_uint8_t fsr,MODULE_MPU9250* module){
	MPU_Write_Byte(MPU9250_ADDR,MPU_ACCEL_CFG_REG,fsr<<3,module);
}

//设置MPU9250的数字低通滤波器
//lpf:数字低通滤波频率(Hz)
static void MPU_Set_LPF(rt_uint16_t lpf,MODULE_MPU9250* module){
	rt_uint8_t data=0;
	if(lpf>=188)data=1;
	else if(lpf>=98)data=2;
	else if(lpf>=42)data=3;
	else if(lpf>=20)data=4;
	else if(lpf>=10)data=5;
	else data=6; 
	MPU_Write_Byte(MPU9250_ADDR,MPU_CFG_REG,data,module);
}

//设置MPU9250的采样率(假定Fs=1KHz)
//rate:4~1000(Hz) 
static void MPU_Set_Rate(rt_uint16_t rate,MODULE_MPU9250* module)
{
	rt_uint8_t data;
	if(rate>1000)rate=1000;
	if(rate<4)rate=4;
	data=1000/rate-1;
	MPU_Write_Byte(MPU9250_ADDR,MPU_SAMPLE_RATE_REG,data,module);
 	MPU_Set_LPF(rate/2,module);	//自动设置LPF为采样率的一半
}

//得到温度值
//返回值:温度值(扩大了100倍)
static void Module_GetTemperature(MODULE_MPU9250* module){
    rt_uint8_t buf[2]; 
    short raw;
	MPU_Read_Len(MPU9250_ADDR,MPU_TEMP_OUTH_REG,2,buf,module); 
    raw=((rt_uint16_t)buf[0]<<8)|buf[1];  
    module->ChipTemperature = 21+((double)raw)/333.87;
}

//得到陀螺仪值(原始值)
//gx,gy,gz:陀螺仪x,y,z轴的原始读数(带符号)
static void Module_GetOringalGyro(MODULE_MPU9250* module)
{
    rt_uint8_t buf[6]; 
	MPU_Read_Len(MPU9250_ADDR,MPU_GYRO_XOUTH_REG,6,buf,module);

	module->OringalGyroscope[X_AXIS]=((rt_uint16_t)buf[0]<<8)|buf[1];  
	module->OringalGyroscope[Y_AXIS]=((rt_uint16_t)buf[2]<<8)|buf[3];  
	module->OringalGyroscope[Z_AXIS]=((rt_uint16_t)buf[4]<<8)|buf[5];
}

//得到加速度值(原始值)
//gx,gy,gz:陀螺仪x,y,z轴的原始读数(带符号)
static void Module_GetOringalAccel(MODULE_MPU9250* module)
{
    rt_uint8_t buf[6];  
	MPU_Read_Len(MPU9250_ADDR,MPU_ACCEL_XOUTH_REG,6,buf,module);

	module->OringalAccelerometer[X_AXIS]=((rt_uint16_t)buf[0]<<8)|buf[1];  
	module->OringalAccelerometer[Y_AXIS]=((rt_uint16_t)buf[2]<<8)|buf[3];  
	module->OringalAccelerometer[Z_AXIS]=((rt_uint16_t)buf[4]<<8)|buf[5];
}

//得到磁力计值(原始值)
//mx,my,mz:磁力计x,y,z轴的原始读数(带符号)
static void Module_GetOringalMag(MODULE_MPU9250* module)
{
    rt_uint8_t buf[6];  
	MPU_Read_Len(AK8963_ADDR,MAG_XOUT_L,6,buf,module);

	module->OringalMagnetometer[X_AXIS]=((rt_uint16_t)buf[1]<<8)|buf[0];  
	module->OringalMagnetometer[Y_AXIS]=((rt_uint16_t)buf[3]<<8)|buf[2];  
	module->OringalMagnetometer[Z_AXIS]=((rt_uint16_t)buf[5]<<8)|buf[4];
 	
    MPU_Write_Byte(AK8963_ADDR,MAG_CNTL1,0X11,module); //AK8963每次读完以后都需要重新设置为单次测量模式
}

//IIC连续读
//addr:器件地址
//reg:要读取的寄存器地址
//len:要读取的长度
//buf:读取到的数据存储区
static void MPU_Read_Len(rt_uint8_t addr,rt_uint8_t reg,rt_uint8_t len,rt_uint8_t *buf,MODULE_MPU9250* module)
{ 
	rt_i2c_master_send(module->I2C_DevHandle, addr, 0, &reg, 1);
	rt_i2c_master_recv(module->I2C_DevHandle, addr, 0, buf, len);
}

//IIC写一个字节 
//devaddr:器件IIC地址
//reg:寄存器地址
//data:数据
static void MPU_Write_Byte(rt_uint8_t addr,rt_uint8_t reg,rt_uint8_t data,MODULE_MPU9250* module)
{
    rt_uint8_t buf[2];

    buf[0] = reg;
    buf[1] = data;
    rt_i2c_master_send(module->I2C_DevHandle, addr, 0, buf ,2);
}

//IIC读一个字节 
//reg:寄存器地址 
//返回值:读到的数据
static rt_uint8_t MPU_Read_Byte(rt_uint8_t addr,rt_uint8_t reg,MODULE_MPU9250* module)
{
	rt_uint8_t buf;
	rt_i2c_master_send(module->I2C_DevHandle, addr, 0, &reg, 1);
	rt_i2c_master_recv(module->I2C_DevHandle, addr, 0, &buf, 1);
	return buf;
}



/************************ (C) COPYRIGHT 2020 WANGXI **************END OF FILE****/
