/*
 * Copyright (c) 2020 - ~, HIT_HERO Team
 *
 * UART HMI MODULE HEAD FILE
 * Used in RT-Thread Operate System
 *
 * Change Logs:
 * Date           Author       Notes                Mail
 * 2020-09-06     WangXi       first version        WangXi_chn@foxmail.com
 */
#ifndef _MODULE_UARTHMI_
#define _MODULE_UARTHMI_
 
#include <rtthread.h>
#include <rtdevice.h>
#include <board.h>

#define UARTHMI_FRAME_LENGTH 11
 
struct _MODULE_UARTHMI
{
    /* Property */
    char *              Property_UartDevName; 
	rt_uint8_t 			Property_MyID;
	
    /* Value */
    rt_device_t         Value_uart_dev;
	rt_uint8_t          Value_feedData[UARTHMI_FRAME_LENGTH];
	rt_uint8_t 			Value_GetID;

    
    /* Method */
    void (*Method_Init)(struct _MODULE_UARTHMI *module);
    void (*Method_Send)(struct _MODULE_UARTHMI *module,char* string);
	void (*Method_Feed)(struct _MODULE_UARTHMI *module,rt_uint8_t* packetID,rt_int32_t* param);
	rt_err_t (*Method_Handle)(rt_device_t dev, rt_size_t size);
};
typedef struct _MODULE_UARTHMI MODULE_UARTHMI;
 
 
void Module_UartHMI_Config(MODULE_UARTHMI *module);
 
 
 
 
#endif
/************************ (C) COPYRIGHT 2020 WANGXI **************END OF FILE****/
