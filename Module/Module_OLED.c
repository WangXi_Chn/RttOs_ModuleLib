/*
 * Copyright (c) 2020 - ~, HIT_HERO Team
 *
 * OLED MODULE SOUCE FILE
 * Used in RT-Thread Operate System
 *
 * Change Logs:
 * Date           Author       Notes            	Mail
 * 2020-09-04     WangXi       first version    	WangXi_chn@foxmail.com
 */
 
#include "Module_OLED.h"


/* User Finsh Code Begin*/



/* User Finsh Code End */

/* Static Method */
static void Module_OledInit		(MODULE_OLED *module);
static void Method_OledSetPos	(MODULE_OLED *module,rt_uint8_t x, rt_uint8_t y);
static void Method_OledFill		(MODULE_OLED *module,rt_uint8_t fill_Data);
static void Method_OledCls		(MODULE_OLED *module);
static void Method_OledWakeUp	(MODULE_OLED *module);
static void Method_OledSleep	(MODULE_OLED *module);
static void Method_OledShowStr	(MODULE_OLED *module,rt_uint8_t x, rt_uint8_t y,char ch[], rt_uint8_t TextSize);
static void Method_OledShowBmp	(MODULE_OLED *module,rt_uint8_t x0, rt_uint8_t y0,rt_uint8_t x1, rt_uint8_t y1,rt_uint8_t BMP[]);


static void IICStart(MODULE_OLED* module);
static void IICStop(MODULE_OLED* module);
static void IICWaitAck(MODULE_OLED* module);
static void WriteDat(MODULE_OLED* module,rt_uint8_t I2C_Data);
static void WriteCmd(MODULE_OLED* module,rt_uint8_t I2C_Command);
static void WriteByte(MODULE_OLED* module,rt_uint8_t Byte);


/* Global Method */
rt_err_t Module_Oled_Config(MODULE_OLED *Dev_OLED){
    if(	Dev_OLED->Method_Init		==	NULL	&&
		Dev_OLED->Method_Cls		==  NULL    &&
		Dev_OLED->Method_Fill		==	NULL	&&
		Dev_OLED->Method_SetPos		==	NULL	&&
		Dev_OLED->Method_ShowBmp	==	NULL	&&
		Dev_OLED->Method_ShowStr	==	NULL	&&
		Dev_OLED->Method_Sleep		==  NULL	&&
		Dev_OLED->Method_WakeUp		==	NULL
	
    ){  
        /* Link the Method */
        Dev_OLED->Method_Init 		= Module_OledInit;		
		Dev_OLED->Method_Cls		= Method_OledCls;	
		Dev_OLED->Method_Fill		= Method_OledFill;		
		Dev_OLED->Method_SetPos		= Method_OledSetPos;		
		Dev_OLED->Method_ShowBmp	= Method_OledShowBmp;	
		Dev_OLED->Method_ShowStr	= Method_OledShowStr;	
		Dev_OLED->Method_Sleep		= Method_OledSleep;	
		Dev_OLED->Method_WakeUp		= Method_OledWakeUp;	
		
    }
    else{
        rt_kprintf("Warning: Module Oled is Configed twice\n");
        return RT_ERROR;
    }

    /* Device Init */
    Dev_OLED->Method_Init(Dev_OLED);
    return RT_EOK;
}

/* Static Method */
static void Module_OledInit(MODULE_OLED *module)
{
	rt_pin_mode(module->Property_SCKpin, PIN_MODE_OUTPUT );
	rt_pin_mode(module->Property_SCKpin, PIN_MODE_OUTPUT );
	
	rt_thread_mdelay(500);      //初始化之前的延时很重要！
	
	WriteCmd(module,0xAE);//--turn off oled panel 关闭显示
	WriteCmd(module,0x00);//---set low column address
			
	WriteCmd(module,0x10);//---set high column address
	WriteCmd(module,0x40);//--set start line address  Set Mapping RAM Display Start Line (0x00~0x3F)
			
	WriteCmd(module,0x81);//--set contrast control register设置对比度
			
	WriteCmd(module,0xA0);//--Set SEG/Column Mapping     0xA0左右反置 0xA1正常(显示前预设)
	WriteCmd(module,0xC0);//Set COM/Row Scan Direction   0xC0上下反置 0xC8正常
			
	WriteCmd(module,0xA6);//--set normal display         
	WriteCmd(module,0xA8);//--set multiplex ratio(1 to 64)
			
	WriteCmd(module,0x3F);//--1/64 duty
	WriteCmd(module,0xD3);//-set display offset Shift Mapping RAM Counter (0x00~0x3F)
			
	WriteCmd(module,0x00);//-not offset
	WriteCmd(module,0xD5);//--set display clock divide ratio/oscillator frequency
			
	WriteCmd(module,0x80);//--set divide ratio, Set Clock as 100 Frames/Sec
	WriteCmd(module,0xD9);//--set pre-charge period
	WriteCmd(module,0xF1);//Set Pre-Charge as 15 Clocks & Discharge as 1 Clock
	WriteCmd(module,0xDA);//--set com pins hardware configuration
	WriteCmd(module,0x12);
	WriteCmd(module,0xDB);//--set vcomh
	WriteCmd(module,0x40);//Set VCOM Deselect Level
	WriteCmd(module,0x20);//-Set Page Addressing Mode (0x00/0x01/0x02)
	WriteCmd(module,0x02);//
	WriteCmd(module,0x8D);//--set Charge Pump enable/disable
	WriteCmd(module,0x14);//--set(0x10) disable
	WriteCmd(module,0xA4);// Disable Entire Display On  0xA4全显 0xA5部分显示
	WriteCmd(module,0xA6);// Disable Inverse Display On 0xA6正显 0xA7反显
	WriteCmd(module,0xAF);//--turn on oled panel 开启显示      
	module->Method_Fill(module,0x00); //初始清屏
	module->Method_SetPos(module,0,0);

}

static void Method_OledSetPos(MODULE_OLED *module,rt_uint8_t x, rt_uint8_t y)
{
	WriteCmd(module,0xb0+y);
	WriteCmd(module,((x&0xf0)>>4)|0x10);
	WriteCmd(module,(x&0x0f)|0x01);	
}

static void Method_OledFill(MODULE_OLED *module,rt_uint8_t fill_Data)
{
	uint8_t y,x;
	for(y=0;y<module->Property_Y_WIDTH/4;y++)
	{
		WriteCmd(module,0xb0+y);
		WriteCmd(module,0x01);
		WriteCmd(module,0x10);
		for(x=0;x<module->Property_X_WIDTH;x++)
			WriteDat(module,fill_Data);
	}
}

static void Method_OledCls(MODULE_OLED *module)
{
	module->Method_Fill(module,0x00);
}

static void Method_OledWakeUp(MODULE_OLED *module)
{
	WriteCmd(module,0X8D);  //设置电荷泵
	WriteCmd(module,0X14);  //开启电荷泵
	WriteCmd(module,0XAF);  //OLED唤醒
}

static void Method_OledSleep(MODULE_OLED *module)
{
	WriteCmd(module,0X8D);  //设置电荷泵
	WriteCmd(module,0X10);  //关闭电荷泵
	WriteCmd(module,0XAE);  //OLED休眠
}

static void Method_OledShowStr(MODULE_OLED *module,rt_uint8_t x, rt_uint8_t y,char ch[], rt_uint8_t TextSize)
{
	unsigned char c = 0,i = 0,j = 0;
	switch(TextSize)
	{
		case 1:
		{
			while(ch[j] != '\0')
			{
				c = ch[j] - 32;
				if(x > 126)
				{
					x = 0;
					y++;
				}
				module->Method_SetPos(module,x,y);
				for(i=0;i<6;i++)
					WriteDat(module,F6x8[c][i]);
				x += 6;
				j++;
			}
		}break;
		case 2:
		{
			while(ch[j] != '\0')
			{
				c = ch[j] - 32;
				if(x > 120)
				{
					x = 0;
					y++;
				}
				module->Method_SetPos(module,x,y);
				for(i=0;i<8;i++)
					WriteDat(module,F8X16[c*16+i]);
				module->Method_SetPos(module,x,y+1);
				for(i=0;i<8;i++)
					WriteDat(module,F8X16[c*16+i+8]);
				x += 8;
				j++;
			}
		}break;
	}
}

static void Method_OledShowBmp(MODULE_OLED *module,rt_uint8_t x0, rt_uint8_t y0,rt_uint8_t x1, rt_uint8_t y1,rt_uint8_t BMP[])
{
	unsigned int j=0;
	unsigned char x,y;

	if(y1%8==0)
		y = y1/8;
	else
		y = y1/8 + 1;
	for(y=y0;y<y1;y++)
	{
		module->Method_SetPos(module,x0,y);
		for(x=x0;x<x1;x++)
		{
			WriteDat(module,BMP[j++]);
		}
	}
}

static void WriteByte(MODULE_OLED* module,rt_uint8_t Byte)
{
	unsigned char i;
	unsigned char m,da;
	da=Byte;
	rt_pin_write(module->Property_SCKpin, PIN_LOW);//SDA IIC接口的时钟信号
	for(i=0;i<8;i++)
	{
		m=da;
		m=m&0x80;
		if(m==0x80)
			rt_pin_write(module->Property_SDApin, PIN_HIGH);		
		else 
			rt_pin_write(module->Property_SDApin, PIN_LOW);
		da=da<<1;
		rt_pin_write(module->Property_SCKpin, PIN_HIGH);
		rt_pin_write(module->Property_SCKpin, PIN_LOW);
	}
}

static void WriteCmd(MODULE_OLED* module,rt_uint8_t I2C_Command)
{
	IICStart(module);
	WriteByte(module,0x78); //Slave address,SA0=0
	IICWaitAck(module);
	WriteByte(module,0x00); //write command
	IICWaitAck(module);
	WriteByte(module,I2C_Command); 
	IICWaitAck(module);
	IICStop(module);
}

static void WriteDat(MODULE_OLED* module,rt_uint8_t I2C_Data)
{
	IICStart(module);
	WriteByte(module,0x78); //D/C#=0; R/W#=0
	IICWaitAck(module);
	WriteByte(module,0x40); //write data
	IICWaitAck(module);
	WriteByte(module,I2C_Data);
	IICWaitAck(module);
	IICStop(module);
	
}

static void IICStart(MODULE_OLED* module)
{
	rt_pin_write(module->Property_SCKpin, PIN_HIGH);
	rt_pin_write(module->Property_SDApin, PIN_HIGH);
	rt_pin_write(module->Property_SDApin, PIN_LOW);
	rt_pin_write(module->Property_SCKpin, PIN_LOW);
}

static void IICStop(MODULE_OLED* module)
{
	rt_pin_write(module->Property_SCKpin, PIN_HIGH);
	rt_pin_write(module->Property_SDApin, PIN_LOW);
	rt_pin_write(module->Property_SDApin, PIN_HIGH);
}

static void IICWaitAck(MODULE_OLED* module)
{
	rt_pin_write(module->Property_SCKpin, PIN_HIGH) ;
	rt_pin_write(module->Property_SCKpin, PIN_LOW);
}

/************************ (C) COPYRIGHT 2020 WANGXI **************END OF FILE****/
