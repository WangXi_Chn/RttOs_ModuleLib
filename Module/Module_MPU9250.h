/*
 * Copyright (c) 2020 - ~, HIT_HERO Team
 *
 * MPU9250 MODULE HEAD FILE
 * Used in RT-Thread Operate System
 *
 * Change Logs:
 * Date           Author       Notes			Mail
 * 2020-09-04     WangXi   	   first version	WangXi_Chn@foxmail.com
 */
 
#ifndef _MODULE_MPU9250_H_
#define _MODULE_MPU9250_H_

#include <rtthread.h>
#include <rtdevice.h>
#include <board.h>

#include "Math_Kalman.h"

typedef enum{
	RANGE360MODE = 0,
	RANGE180MODE
}
_GPS_ANGLEMODE;

enum AIXS{
	X_AXIS = 0,
	Y_AXIS,
	Z_AXIS
};

enum GYRO_RANGE{
	_250DPS = 0,
	_500DPS,
	_1000DPS,
	_2000DPS
};

enum ACCEL_RANGE{
	_2G = 0,
	_4G,
	_8G,
	_16G
};

struct _MODULE_MPU9250
{
	/* Property */
	char *I2CbusName;								/*!< Specifies the I2C Bus Used to be configured.
													This parameter is defined by RT-Thread Operate System rtconfig*/
	
	MATH_KALMANFILTER 	GyroZaxisFilter;
	
	rt_uint8_t 			SWICH_DEBUG;				/* If SWITH_DEBUG is 1, print the value to Finsh */
	_GPS_ANGLEMODE 		GPS_ANGLEMODE;				/* Chose the module return data range 0~360/-180~+180 */
	
	/* Value */
	struct rt_i2c_bus_device *I2C_DevHandle;		
	

	
	rt_int16_t ChipTemperature;
	
	rt_int16_t OringalGyroscope[3];
	rt_int16_t OringalAccelerometer[3];
	rt_int16_t OringalMagnetometer[3];
	
	rt_tick_t tick_now;
	rt_tick_t tick_last;
	
	rt_int16_t OffsetCulCount;
	rt_int16_t OffsetGyroscope[3];
	rt_int16_t OffsetAccelerometer[3];
	float OffsetMagnetometer[3];
	float OffrateMagnetometer[3];                 //地磁计圆化比率x/y
	
	float AngleGyroscope[3];
	float AngleAccelerometer[3];
	float AngleMagnetometer[3];
	float Angle[3];
	
	/* Method */
	rt_err_t (*Init)(struct _MODULE_MPU9250 *module);
	
	void (*GetChipTemperature)(struct _MODULE_MPU9250 *module);
	
	void (*GetOringalGyro)(struct _MODULE_MPU9250 *module);
	void (*GetOringalAccel)(struct _MODULE_MPU9250 *module);
	void (*GetOringalMag)(struct _MODULE_MPU9250 *module);
	
	void (*GetOffsetGyro)(struct _MODULE_MPU9250 *module);
	
	void (*GetAngleGyro)(struct _MODULE_MPU9250 *module);
};
typedef struct _MODULE_MPU9250 MODULE_MPU9250;

rt_err_t Module_MPU9250_Config(MODULE_MPU9250 *Dev_MPU9250);


/*----------------MPU9250 REGISTER DATA-----------------------*/

//如果AD0脚(9脚)接地,IIC地址为0X68(不包含最低位).
//如果接V3.3,则IIC地址为0X69(不包含最低位).
#define MPU9250_ADDR            0X68    //MPU6500的器件IIC地址
#define MPU6500_ID				0X71  	//MPU6500的器件ID

//MPU9250内部封装了一个AK8963磁力计,地址和ID如下:
#define AK8963_ADDR				0X0C	//AK8963的I2C地址
#define AK8963_ID				0X48	//AK8963的器件ID

//AK8963的内部寄存器
#define MAG_WIA					0x00	//AK8963的器件ID寄存器地址
#define MAG_CNTL1          	  	0X0A    
#define MAG_CNTL2            	0X0B

#define MAG_XOUT_L				0X03	
#define MAG_XOUT_H				0X04
#define MAG_YOUT_L				0X05
#define MAG_YOUT_H				0X06
#define MAG_ZOUT_L				0X07
#define MAG_ZOUT_H				0X08

//MPU6500的内部寄存器
#define MPU_SELF_TESTX_REG		0X0D	//自检寄存器X
#define MPU_SELF_TESTY_REG		0X0E	//自检寄存器Y
#define MPU_SELF_TESTZ_REG		0X0F	//自检寄存器Z
#define MPU_SELF_TESTA_REG		0X10	//自检寄存器A
#define MPU_SAMPLE_RATE_REG		0X19	//采样频率分频器
#define MPU_CFG_REG				0X1A	//配置寄存器
#define MPU_GYRO_CFG_REG		0X1B	//陀螺仪配置寄存器
#define MPU_ACCEL_CFG_REG		0X1C	//加速度计配置寄存器
#define MPU_MOTION_DET_REG		0X1F	//运动检测阀值设置寄存器
#define MPU_FIFO_EN_REG			0X23	//FIFO使能寄存器
#define MPU_I2CMST_CTRL_REG		0X24	//IIC主机控制寄存器
#define MPU_I2CSLV0_ADDR_REG	0X25	//IIC从机0器件地址寄存器
#define MPU_I2CSLV0_REG			0X26	//IIC从机0数据地址寄存器
#define MPU_I2CSLV0_CTRL_REG	0X27	//IIC从机0控制寄存器
#define MPU_I2CSLV1_ADDR_REG	0X28	//IIC从机1器件地址寄存器
#define MPU_I2CSLV1_REG			0X29	//IIC从机1数据地址寄存器
#define MPU_I2CSLV1_CTRL_REG	0X2A	//IIC从机1控制寄存器
#define MPU_I2CSLV2_ADDR_REG	0X2B	//IIC从机2器件地址寄存器
#define MPU_I2CSLV2_REG			0X2C	//IIC从机2数据地址寄存器
#define MPU_I2CSLV2_CTRL_REG	0X2D	//IIC从机2控制寄存器
#define MPU_I2CSLV3_ADDR_REG	0X2E	//IIC从机3器件地址寄存器
#define MPU_I2CSLV3_REG			0X2F	//IIC从机3数据地址寄存器
#define MPU_I2CSLV3_CTRL_REG	0X30	//IIC从机3控制寄存器
#define MPU_I2CSLV4_ADDR_REG	0X31	//IIC从机4器件地址寄存器
#define MPU_I2CSLV4_REG			0X32	//IIC从机4数据地址寄存器
#define MPU_I2CSLV4_DO_REG		0X33	//IIC从机4写数据寄存器
#define MPU_I2CSLV4_CTRL_REG	0X34	//IIC从机4控制寄存器
#define MPU_I2CSLV4_DI_REG		0X35	//IIC从机4读数据寄存器

#define MPU_I2CMST_STA_REG		0X36	//IIC主机状态寄存器
#define MPU_INTBP_CFG_REG		0X37	//中断/旁路设置寄存器
#define MPU_INT_EN_REG			0X38	//中断使能寄存器
#define MPU_INT_STA_REG			0X3A	//中断状态寄存器

#define MPU_ACCEL_XOUTH_REG		0X3B	//加速度值,X轴高8位寄存器
#define MPU_ACCEL_XOUTL_REG		0X3C	//加速度值,X轴低8位寄存器
#define MPU_ACCEL_YOUTH_REG		0X3D	//加速度值,Y轴高8位寄存器
#define MPU_ACCEL_YOUTL_REG		0X3E	//加速度值,Y轴低8位寄存器
#define MPU_ACCEL_ZOUTH_REG		0X3F	//加速度值,Z轴高8位寄存器
#define MPU_ACCEL_ZOUTL_REG		0X40	//加速度值,Z轴低8位寄存器

#define MPU_TEMP_OUTH_REG		0X41	//温度值高八位寄存器
#define MPU_TEMP_OUTL_REG		0X42	//温度值低8位寄存器

#define MPU_GYRO_XOUTH_REG		0X43	//陀螺仪值,X轴高8位寄存器
#define MPU_GYRO_XOUTL_REG		0X44	//陀螺仪值,X轴低8位寄存器
#define MPU_GYRO_YOUTH_REG		0X45	//陀螺仪值,Y轴高8位寄存器
#define MPU_GYRO_YOUTL_REG		0X46	//陀螺仪值,Y轴低8位寄存器
#define MPU_GYRO_ZOUTH_REG		0X47	//陀螺仪值,Z轴高8位寄存器
#define MPU_GYRO_ZOUTL_REG		0X48	//陀螺仪值,Z轴低8位寄存器

#define MPU_I2CSLV0_DO_REG		0X63	//IIC从机0数据寄存器
#define MPU_I2CSLV1_DO_REG		0X64	//IIC从机1数据寄存器
#define MPU_I2CSLV2_DO_REG		0X65	//IIC从机2数据寄存器
#define MPU_I2CSLV3_DO_REG		0X66	//IIC从机3数据寄存器

#define MPU_I2CMST_DELAY_REG	0X67	//IIC主机延时管理寄存器
#define MPU_SIGPATH_RST_REG		0X68	//信号通道复位寄存器
#define MPU_MDETECT_CTRL_REG	0X69	//运动检测控制寄存器
#define MPU_USER_CTRL_REG		0X6A	//用户控制寄存器
#define MPU_PWR_MGMT1_REG		0X6B	//电源管理寄存器1
#define MPU_PWR_MGMT2_REG		0X6C	//电源管理寄存器2 
#define MPU_FIFO_CNTH_REG		0X72	//FIFO计数寄存器高八位
#define MPU_FIFO_CNTL_REG		0X73	//FIFO计数寄存器低八位
#define MPU_FIFO_RW_REG			0X74	//FIFO读写寄存器
#define MPU_DEVICE_ID_REG		0X75	//器件ID寄存器


#endif

/************************ (C) COPYRIGHT 2020 WANGXI **************END OF FILE****/

