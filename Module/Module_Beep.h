/*
 * Copyright (c) 2020 - ~, HIT_HERO Team
 *
 * BEEP MODULE HEAD FILE
 * Used in RT-Thread Operate System
 *
 * Change Logs:
 * Date           Author       Notes            	Mail
 * 2020-07-31     WangXi       second version    	WangXi_chn@foxmail.com
 */
#ifndef _MODULE_BEEP_H_
#define _MODULE_BEEP_H_

enum _BEEP_STATION
{
	MODULE_BEEP_ON = 0,
	MODULE_BEEP_OFF
};
typedef enum _BEEP_STATION BEEP_STATION ;

#include <rtthread.h>
#include <rtdevice.h>
#include <board.h>

struct _MODULE_BEEP
{
    /* Property */
	char * PwmDevName;
		
	rt_uint32_t PWM_CHANNEL;	
	rt_uint32_t PWM_FREQ;
	rt_uint32_t PWM_DUTY;
	
	rt_uint32_t BEEP_TIME_CYCLE;
	rt_uint32_t BEEP_TIME_OUTPUT;  

	/* Value */
	struct rt_device_pwm *pwm_dev;
	rt_uint16_t last_time_show_cycle;  	// 状态查询时间
    rt_uint16_t set_time_cycle;        	// 循环时间
    rt_uint16_t set_last_time;         	// 上一次电平输出时间
    rt_uint8_t  curr_number;        	// 当前
    rt_uint8_t  next_number;           	// 下一个指示次数
	
	/* Method */
    void (*Init)(struct _MODULE_BEEP *module);
	void (*Loop)(struct _MODULE_BEEP *module);
	void (*Set)(struct _MODULE_BEEP *module,rt_uint8_t number);
	void (*Control)(struct _MODULE_BEEP *module,BEEP_STATION station);
};
typedef struct _MODULE_BEEP MODULE_BEEP;

/* Glodal Method */
rt_err_t Module_Beep_Config(MODULE_BEEP *Dev_Beep);

#endif 
/************************ (C) COPYRIGHT 2020 WANGXI **************END OF FILE****/

