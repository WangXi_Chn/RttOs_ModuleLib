/*
 * Copyright (c) 2020 - ~, HIT_HERO Team
 *
 * UARTMAVLINK MODULE SOUCE FILE
 * Used in RT-Thread Operate System
 * Used in MAVLink Protocol
 *
 * Change Logs:
 * Date           Author       Notes                Mail
 * 2020-09-26     WangXi       first version        WangXi_chn@foxmail.com
 */
 
#include "Module_UartMAVLink.h"

static void     Module_UartMavlinkInit(MODULE_UARTMAVLINK *module);
static void     Module_UartMavlinkSend(MODULE_UARTMAVLINK *module);
static void     Module_UartMavlinkFeed(MODULE_UARTMAVLINK *module);
static rt_err_t Module_UartMavlinkHandle(rt_device_t dev, rt_size_t size);

static struct rt_semaphore uartMavlink_sem;


static void		Module_UartMavlinkJC24BConfig(MODULE_UARTMAVLINK *module);

/* Global Method */
rt_err_t Module_UartMavlink_Config(MODULE_UARTMAVLINK *module)
{
    if( module->Method_Init     ==NULL &&
        module->Method_Handle   ==NULL &&
        module->Method_Send     ==NULL &&
        module->Method_Feed     ==NULL
                
    ){  
        /* Link the Method */
        module->Method_Init         = Module_UartMavlinkInit;
        module->Method_Handle       = Module_UartMavlinkHandle;
        module->Method_Send         = Module_UartMavlinkSend;
        module->Method_Feed         = Module_UartMavlinkFeed;
        
    }
    else{
        rt_kprintf("Warning: Module uart Communication is Configed twice\n");
        return RT_ERROR;
    }

    /* Device Init */
    module->Method_Init(module);

    return RT_EOK;
}

static void Module_UartMavlinkInit(MODULE_UARTMAVLINK *module)
{
    module->Value_uart_dev = rt_device_find(module->Property_UartDevName);
    if (!module->Value_uart_dev)
    {
        rt_kprintf("find %s failed!\t\n", module->Value_uart_dev);
        return;
    }
    
    rt_sem_init(&uartMavlink_sem, "uartCom_sem", 0, RT_IPC_FLAG_FIFO);

    /* 初始化消息队列 */
    rt_mq_init(&(module->Value_mavlinkmsg_queue),"mavlinkq",module->Value_mavlinkmsg_pool,
               sizeof(module->Value_mavlinkgetmsg),                      	
               sizeof(module->Value_mavlinkmsg_pool),       
               RT_IPC_FLAG_FIFO);       
	
	
    struct serial_configure config = RT_SERIAL_CONFIG_DEFAULT;  /* 初始化配置参数 */
	
    /* step2：修改串口配置参数 */
	if(module->Property_BaudRate == 9600)
	{
		config.baud_rate = BAUD_RATE_9600;        //修改波特率为 9600
	}
	else if(module->Property_BaudRate == 115200)
	{
		config.baud_rate = BAUD_RATE_115200;       //修改波特率为 115200
	}
    else
	{
		rt_kprintf("warning: Uartcom's baudrate %d set failed, will be default value 115200\t\n",module->Property_BaudRate);
	}
	
    config.data_bits = DATA_BITS_8;           //数据位 8
    config.stop_bits = STOP_BITS_1;           //停止位 1
    config.bufsz     = 128;                   //修改缓冲区 buff size 为 128
    config.parity    = PARITY_NONE;           //无奇偶校验位
    
    /* step3：控制串口设备。通过控制接口传入命令控制字，与控制参数 */
    rt_device_control(module->Value_uart_dev, RT_DEVICE_CTRL_CONFIG, &config);  
    rt_device_open(module->Value_uart_dev, RT_DEVICE_FLAG_DMA_RX);
    rt_device_set_rx_indicate(module->Value_uart_dev, module->Method_Handle);
	
	if(module->Property_Device == JC24B)
	{
		Module_UartMavlinkJC24BConfig(module);
	}
    
}

static void Module_UartMavlinkSend(MODULE_UARTMAVLINK *module)
{	
	
    
}

static void Module_UartMavlinkFeed(MODULE_UARTMAVLINK *module)
{
    uint8_t data;
	uint8_t ret;
		
	while (rt_device_read(module->Value_uart_dev, -1, &data, 1) == 0)
	{
		rt_sem_control(&uartMavlink_sem, RT_IPC_CMD_RESET, RT_NULL);
		rt_sem_take(&uartMavlink_sem, RT_WAITING_FOREVER);
	}
	
	ret = mavlink_parse_char(MAVLINK_COMM_3, data, &(module->Value_mavlinkgetmsg), &(module->Value_mavlinkstatus));
	
	if(MAVLINK_FRAMING_OK == ret)
		rt_mq_send(&(module->Value_mavlinkmsg_queue), &(module->Value_mavlinkgetmsg), sizeof(module->Value_mavlinkgetmsg));

}

static rt_err_t Module_UartMavlinkHandle(rt_device_t dev, rt_size_t size)
{   
    rt_sem_release(&uartMavlink_sem);

    return RT_EOK;
}

static void	Module_UartMavlinkJC24BConfig(MODULE_UARTMAVLINK *module)
{
	char Config_data[18];
	
	rt_pin_mode(module->Property_JC24BsetPin, PIN_MODE_OUTPUT);
	rt_pin_write(module->Property_JC24BsetPin, PIN_LOW);
	
	//0	   1    2   3    4   5    6    7      8    9        10    11     12   13     14   15    16    17
	//AA   5A	00  00   00  10   00   00     00   04       00	  1F	 00	  01     00	  12	00	  06
	//0xaa 0x5a 模块ID   组网ID   0x00 RF功率 0x00 波特率   0x00  RF信道 0x00 速率   0x00 0x12  0x00  和校验
	rt_thread_mdelay(1);

	Config_data[0] = 0xAA;
	Config_data[1] = 0x5A;
	Config_data[2] = 0x00;
	Config_data[3] = module->Property_MyID;
	Config_data[4] = 0x00;
	Config_data[5] = module->Property_NetID;
	Config_data[6] = 0x00;
	Config_data[7] = 0x00;
	Config_data[8] = 0x00;
	Config_data[9] = 0x04;
	Config_data[10] = 0x00;
	Config_data[11] = 0x1F;
	Config_data[12] = 0x00;
	Config_data[13] = 0x01;
	Config_data[14] = 0x00;
	Config_data[15] = 0x12;
	Config_data[16] = 0x00;
	Config_data[17] = 0x00;
	for(int i = 0;i<17;i++)
		Config_data[17] += Config_data[i];

	rt_device_write(	module->Value_uart_dev, 0, 
						Config_data, sizeof(Config_data));
						
	rt_thread_mdelay(1);
				
	rt_pin_write(module->Property_JC24BsetPin, PIN_HIGH);
}


/************************ (C) COPYRIGHT 2020 WANGXI **************END OF FILE****/

