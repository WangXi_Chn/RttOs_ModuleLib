/*
 * Copyright (c) 2020 - ~, HIT_HERO Team
 *
 * PULSE ENCODER MODULE HEAD FILE
 * Used in RT-Thread Operate System
 *
 * Change Logs:
 * Date           Author       Notes                Mail
 * 2020-09-03     WangXi       first version        WangXi_chn@foxmail.com
 */
 
#ifndef _MODULE_PULSEENCODER_
#define _MODULE_PULSEENCODER_

#include <rtthread.h>
#include <rtdevice.h>
#include <board.h>

struct _MODULE_PULSEENCODER
{
    /* Property */
    char *              Property_PulseDevName; 
    rt_base_t 			Property_CH1_pin;
	rt_base_t 			Property_CH2_pin;
	
    /* Value */
    rt_device_t         Value_pulse_dev;
	rt_int32_t			Value_pulseNum;
    
    /* Method */
    void (*Method_Init)(struct _MODULE_PULSEENCODER *module);
	void (*Method_Read)(struct _MODULE_PULSEENCODER *module);
    void (*Method_Clear)(struct _MODULE_PULSEENCODER *module);
	
};
typedef struct _MODULE_PULSEENCODER MODULE_PULSEENCODER;

rt_err_t Module_PulseEncoder_Config(MODULE_PULSEENCODER *module);

#endif
/************************ (C) COPYRIGHT 2020 WANGXI **************END OF FILE****/
